\Appendix{Proof of Equivalence Between Two Models}
%%\UCTD{\tensor{D}}
\section{Rajagopal et al.}

As discussed in Chapter 3, the Oldroyd-B fluid model derived in the paper written by Rajagopal et al. in 2000 is written in the following form:
%%
\begin{align}
	\label{eq:OldStress}
	\tensor{T}&=-p\tensor{I}+\tensor{S},
	\\
	\label{eq:OldS}
	\tensor{S}&=\mu\tensor{B}_{\kappa_{p(t)}}+\eta_{1}\tensor{D},
	\\
	\label{eq:OldSDeriv}
	\tensor{S}+\frac{\eta}{2\mu}\UCTD{\tensor{S}}&=\eta_{1}\biggl(\tensor{D}+\frac{\eta}{2\mu}\UCTD{\tensor{D}}\biggr)+\mu\lambda\tensor{I}.
\end{align}
%%

Substituting $\tensor{S}$ into \ref{eq:OldStress} and \ref{eq:OldSDeriv}, we have,

%%
\begin{align}
	\label{eq:OldStress2B}
	\tensor{T}&=-p\tensor{I}+\eta_{1}\tensor{D}+\mu\tensor{B}_{\kappa_{p(t)}}\\
	\label{eq:OldSDeriv2B}
	(\eta_{1}\tensor{D}+\mu\tensor{B}_{\kappa_{p(t)}})+\frac{\eta}{2\mu}\UCTD{\overline{(\eta_{1}\tensor{D}+\mu\tensor{B}_{\kappa_{p(t)}})}}&=\eta_{1}(\tensor{D}+\frac{\eta}{2\mu}\UCTD{\tensor{D}})+\mu\lambda\tensor{I}
\end{align}
%%

Distributing the upper-convected derivative on the left-hand-side of \ref{eq:OldSDeriv2B} gives,

%%
\begin{equation}
	(\eta_{1}\tensor{D}+\mu\tensor{B}_{\kappa_{p(t)}})+\frac{\eta}{2\mu}(\eta_{1}\UCTD{\tensor{D}}+\mu\UCTD{\tensor{B}}_{\kappa_{p(t)}})=\eta_{1}(\tensor{D}+\frac{\eta}{2\mu}\UCTD{\tensor{D}})+\mu\lambda\tensor{I}
	\label{eq:OldSDeriv3B}
\end{equation}
%%

Observe that the two terms involving $\tensor{D}$ show up on both sides of the equation. Canceling these, we find

%%
\begin{equation}
	\mu\tensor{B}_{\kappa_{p(t)}}+\frac{\eta}{2}\UCTD{\tensor{B}}_{\kappa_{p(t)}}=\mu\lambda\tensor{I}
	\label{eq:OldSDeriv4B}
\end{equation}
%%

We can now expand the upper-convected derivate of $\tensor{B}$ according to the definition, $\UCTD{\tensor{A}}=\dot{\tensor{A}}-\tensor{L}\tensor{A}-\tensor{A}\tensor{L}^{T}$, for any arbitrary tensor $\tensor{A}$. From this point on, we will drop the $\kappa_{p(t)}$ subscript on the stress tensor $\tensor{B}$ for efficiency, but it is important to note that we are indeed referring to the stress in the natural configuration.

%%
\begin{equation}
	\mu\tensor{B}+\frac{\eta}{2}(\dot{\tensor{B}}-\tensor{L}\tensor{B}-\tensor{B}\tensor{L}^{T})=\mu\lambda\tensor{I}
	\label{eq:OldSDeriv5B}
\end{equation}
%%

Expanding the material derivative and substituting in $\nabla\bv{v}$ for $\tensor{L}$,

%%
\begin{equation}
	\frac{\partial\tensor{B}}{\partial t}+(\nabla\tensor{B})\bv{v}-(\nabla\bv{v})\tensor{B}-\tensor{B}(\nabla\bv{v})^{T}=\frac{2\mu}{\eta}(\lambda\tensor{I}-\tensor{B})
	\label{eq:OldSDeriv6B}
\end{equation}
%%

Equations \ref{eq:OldStress2B} and \ref{eq:OldSDeriv6B} showcase a simplified version of the model outlined in \cite{Rajagopal2000}.

\section{Hron et al.}

The Oldroyd-B fluid model used in \cite{Hron2014} is written in the following form:

%%
\begin{equation}
	\tensor{T}=-p\tensor{I}+2\eta_{s}\tensor{D}+\mu(\tensor{B}-\tensor{I})
	\label{eq:HronStressB}
\end{equation}
%%
%%
\begin{equation}
	\frac{\partial\tensor{B}}{\partial t}+(\nabla\tensor{B})\bv{v}-(\nabla\bv{v})\tensor{B}-\tensor{B}(\nabla\bv{v})^{T}=\frac{1}{\tau}(\tensor{I}-\tensor{B})
	\label{eq:HronEvolNatConfigB}
\end{equation}
%%

With $\eta_{s}$ being the viscosity of the solvent in the experiment, $\mu$ being the material constant for the elastic modulus, and $\tau$ the relaxation time of the material. When we expand and regroup the terms in \ref{eq:HronStressB}, we find,

%%
\begin{equation}
	\tensor{T}=-(p+\mu)\tensor{I}+2\eta_{s}\tensor{D}+\mu\tensor{B}
	\label{eq:HronStress2B}
\end{equation}
%%

Upon inspection, we can see that this Cauchy stress equation is equivalent to the simplified version of the equation at the end of the previous section (\ref{eq:OldStress2B}). The only differences lie within the values of the constants, but these coefficients themselves represent the same material quantities as in \cite{Rajagopal2000}. As for the evolution of the natural configuration of the fluid, the left-hand-sides of both \ref{eq:OldSDeriv6B} and \ref{eq:HronEvolNatConfigB} are equivalent, but the right-hand-sides have differing coefficients. Using dimension analysis, we can show that these constants represent the same material parameter, henceforth proving that both models are identical. According to the latter equation, the coefficient in front of $(\tensor{I}-\tensor{B})$ should have dimensions of $[t^{-1}]$. We know that $\lambda$ is dimensionless based on its definition ($\lambda=\dfrac{3}{\trace\tensor{B}^{-1}_{\kappa_{p(t)}}}$), $\mu$ has units of $[Pa]$, and $\eta$ has units of $[Pa \cdot s]$, giving the coefficient the same units as in equation \ref{eq:HronEvolNatConfigB}, and proving the models equivalency.