%!TEX root = ../YourName-Dissertation.tex
\chapter{Blood Model \& Study} 

In the previous chapters, we solved our system of equations with a constitutive relationship describing an Oldroyd-B type fluid. Three frames of reference were explored and the results of these calculations were discussed. Now, we introduce the reader to a continuum model for the behavior of blood. The model itself was derived by K.~R.~Rajagopal et.~al in 2000 \cite{Rajagopal2000} and applied to blood by M.~Anand in 2004 \cite{Anand2004Visco}. Following the same procedures as for the Oldroyd-B problem, we will present the governing equations in their Eulerian, Lagrangian and ALE forms. Finally, we will provide details on the various components of the study, the boundary and initial conditions, and other \COMSOL inputs.

\section{Rheological Clot Model}

A continuum model is not an intuitive description for blood or clotted blood. Blood is a \emph{mixture}, made up of red blood cells (erythrocytes), white blood cells (leukocytes), and platelets floating in a colorless fluid called plasma. The constituents comprise about 45\% by volume of healthy human blood. Clotted blood is primarily comprised of platelets within a fibrin matrix, red blood cells, and other nucleated cells (monocytes and neutrophils) with varying concentrations of each \cite{Singh2013}. Though the exact behavior of blood is amazingly complex, as a first approximation, we will neglect the presence of these constituents. Instead, their effects on the blood will be imposed on the fluid model by various material parameters. This way, the computational cost is much lower than that of an anatomically correct model, and the behavior of the continuum aligns reasonably well to experimental data \cite{Anand2004Bio}.

The authors of \cite{Anand2004Visco} represent blood as a viscoelastic fluid with shear-thinning properties (\cite{ChienAgg},\cite{ChienDef}). Starting with the thermodynamic framework presented in \cite{Rajagopal2000}, the stored energy function and scalar dissipation rate are chosen by these authors to have the following form:

%%
\begin{align}
	\label{eq:BloodStoredEnergy}
	\hat{W}&=\frac{\mu}{2}(I-3),
	\\
	\label{eq:BloodRateDissip}
	\hat{\xi}&=\alpha \bigl(\tensor{D}_{\kappa_{p(t)}} \cdot \tensor{B}_{\kappa_{p(t)}} \tensor{D}_{\kappa_{p(t)}}\bigr)^{\gamma}+\eta_{1}\tensor{D} \cdot \tensor{D},
\end{align}
%%

Upon first glance, these assumptions are almost identical to those which resulted in the Oldroyd-B fluid model (Eqs.~\eqref{eq:MaxStoredEnergy} and \eqref{eq:OldRateDissip}); the stored energy function $\hat{W}$ matches that chosen for both the Maxwell and Oldroyd-B model, and the dissipation rate has an identical structure. However, the difference lies in the exponent $\gamma$ on the first term in $\hat{\xi}$. This adds a power-law viscous nature of the fluid, while the second term represents the Newtonian nature of plasma \cite{Anand2004Visco}. These assumptions, after following the maximization procedure in \cite{Rajagopal2000}, result in the following set of constitutive equations:

%%
\begin{align}
	\label{eq:ClotStress}
	\tensor{T}&=-p\tensor{I}+\tensor{S},
	\\
	\label{eq:ClotS}
	\tensor{S}&=\mu\tensor{B}_{\kappa_{p(t)}}+\eta_{1}\tensor{D},
	\\
	\label{eq:ClotBDeriv}
	\UCTD{\tensor{B}}_{\kappa_{p(t)}}&=-2 \bigl(\frac{\mu}{\alpha} \bigr)^{1+2n} \bigl(\text{tr}(\tensor{B}_{\kappa_{p(t)}})-3\lambda \bigr)^{n} \bigl(\tensor{B}_{\kappa_{p(t)}}-\lambda\tensor{I} \bigr),
\end{align}

where

%%
\begin{equation}
	\label{eq:lambda}
	\lambda=\frac{3}{\trace(\tensor{B}_{\kappa_{p(t)}}^{-1})},
\end{equation}
%%

Here, $\mu$ is the shear modulus, $\eta$ is the viscosity, $n$ is the shear-thinning index, and $\alpha$ is a constant that describes the evolution of $\tensor{B}_{\kappa_{p(t)}}$ \cite{Anand2006Form}. Note that this model applies to \emph{clotted} blood as well, with modification to the material parameters. 

Adopting the choice made by the authors of \cite{Anand2004Visco}, we will introduce a constant, K, to replace $\bigl(\frac{\mu}{\alpha} \bigr)^{1+2n}$. The quantity $-2 K \bigl(\text{tr}(\tensor{B}_{\kappa_{p(t)}})-3\lambda \bigr)^{n}$ in Eq.~\eqref{eq:ClotBDeriv} replaces the simplified $\frac{1}{\tau}$ term in Eq.~\eqref{eq:HronOldDeriv} of the Oldroyd-B model. It is for this reason that we call $\bigl[-2 K \bigl(\text{tr}(\tensor{B}_{\kappa_{p(t)}})-3\lambda \bigr)^{n}\bigr]^{-1}$ the relaxation time of the material.

To alleviate the difficulty in inverting the tensor $\tensor{B}$ in the definition of $\lambda$, we apply the Cayley-Hamilton theorem, which states that every square matrix satisfies its own characteristic equation. For symmetric matrices, such as the matrix representation of \tensor{B} in our formulations, the characteristic equation can be written as:

\begin{equation}
	\label{eq:CayHam}
	\tensor{B}^3-\text{I}_B \tensor{B}^2+\text{II}_B \tensor{B}-\text{III}_B \tensor{I}=0,
\end{equation}

where I$_B$, II$_B$, and III$_B$ are the three tensor invariants:

\begin{align}
	\text{I}_B&=\trace(\tensor{B}),
	\\
	\text{II}_B&=\frac{1}{2}\bigl(\trace(\tensor{B})^2-\trace(\tensor{B}^2)\bigr),
	\\
	\text{III}_B&=\det(\tensor{B}).
\end{align} 

Substituting these into Eq.~\eqref{eq:CayHam}, we have:

\begin{equation}
	\label{eq:CayHam}
	\tensor{B}^3-\trace(\tensor{B}) \tensor{B}^2+\frac{1}{2}\bigl(\trace(\tensor{B})^2-\trace(\tensor{B}^2)\bigr) \tensor{B}-\det(\tensor{B}) \tensor{I}=0.
\end{equation}

In our formulation, we define the tensor $\tensor{B}$ to have a determinant equal to 1, simplifying the last term. Rearranging the equation, and multiplying both sides by $\tensor{B}^{-1}$, we obtain:

\begin{equation}
	\frac{1}{2}\bigl(\trace(\tensor{B})^2-\trace(\tensor{B}^2)\bigr) \tensor{I} =\tensor{B}^{-1}-\tensor{B}^2+\trace(\tensor{B}) \tensor{B}.
\end{equation}

We then take the trace of both sides and divide each term by 3.

\begin{align}
	\frac{1}{2}\bigl(\trace(\tensor{B})^2-\trace(\tensor{B}^2)\bigr) &=\frac{1}{3}\trace(\tensor{B}^{-1})+\frac{1}{3}(\trace(\tensor{B})^2-\trace(\tensor{B}^2))
	\\
	\frac{\trace(\tensor{B}^{-1})}{3}&=\frac{1}{6}\bigl(\trace(\tensor{B})^2-\trace(\tensor{B}^2)\bigr).
\end{align}

Therefore, because the matrix representation of the strain tensor \tensor{B} is symmetric with determinant equal to 1, we can say that,

\begin{equation}
	\label{eq:lambdamod}
	\lambda=\frac{3}{\trace(\tensor{B}^{-1})}=\frac{6}{\trace(\tensor{B})^2-\trace(\tensor{B}^2)},
\end{equation}

eliminating the inverse of $\tensor{B}$ from our formulation and alleviating the computational costs.

The problem formulations in Eulerian, Lagrangian, and ALE coordinates are essentially identical to those for the Oldroyd-B example, derived in Chapter~2, with the exception of the altered relaxation time. The Eulerian weak form of the blood-clot problem is 

%%
\begin{align}
	\label{eq:ClotWF1}
	\int_{\Omega_{x}}\trace(\nabla\bv{v})\tilde{p} \, \d{v}&=0,
	\\
	\label{eq:ClotWF2}
	\int_{\Omega_{x}}\rho\biggl[\frac{\partial\bv{v}}{\partial t}+(\nabla\bv{v})\bv{v}\biggr]\cdot\tilde{\bv{v}}\,\d{v}+\int_{\Omega_{x}}\tensor{T}\colon\nabla\tilde{\bv{v}}\, \d{v}-\int_{\partial\Omega_{x}}\tensor{T}\hat{\bv{n}}\cdot\tilde{\bv{v}}\, \d{a}&=0,
	\\
	\int_{\Omega_{x}}\biggl[\frac{\partial\tensor{B}}{\partial t}-(\nabla\bv{v})\tensor{B}-\tensor{B}\transpose{(\nabla\bv{v})}+2 \, \text{K} \, \bigl(\text{tr}(\tensor{B})-3\lambda \bigr)^{n} \bigl(\tensor{B}-\lambda\tensor{I} \bigr)\colon\tilde{\tensor{B}}\, \d{v} \nonumber
	\\
	\label{eq:ClotWF3}
	-\int_{\Omega_{x}}(\tensor{B}\otimes\bv{v})\colon\nabla{\tilde{\tensor{B}}}\, \d{v}+\int_{\partial\Omega_{x}}(\tensor{B}\otimes\bv{v})\hat{\bv{n}}\colon\tilde{\tensor{B}}\, \d{a}&=0,
\end{align}
%%
with
%%
\begin{align}
	\label{eq:CCauchy}
	\tensor{T}&=-p\tensor{I}+\eta(\nabla\bv{v}+\transpose{(\nabla\bv{v})})+\mu(\tensor{B}-\tensor{I})
	\\
	\label{eq:CBDef}
	\tensor{B}&=J_{G}^{-1/3}\tensor{G}
	\\
	\label{eq:CBtDef}
	\frac{\partial\tensor{B}}{\partial t}&=J_{G}^{-1/3}\Bigl[\frac{\partial\tensor{G}}{\partial t}-\frac{1}{3}~\trace\bigl(\tensor{G}^{-1}\frac{\partial\tensor{G}}{\partial t}\bigr)~\tensor{G}\Bigr],
\end{align}
%%

and $\lambda$ given in Eq.~\eqref{eq:lambdamod}. Recall that we have dropped the subscript $\kappa_{p(t)}$ from $\tensor{B}$ for simplicity, but we are in fact referring to the left Cauchy-Green stress tensor from the natural to the current configuration. The quantities $\rho$, $\mu$, $\eta$, n, and K, are constant parameters of our material, with values given in \cite{Anand2004Visco} and \cite{Anand2006Form}. The values of these parameters given in the literature and used in this study are tabulated in Table~\ref{tab:Parameters}.

%
\begin{table}[h!]
\centering
\begin{tabular}{ || c | c || }
	\hline
	Parameter & Value \\ [1ex]
	\hline
	$\rho$ & $\np[\frac{kg}{m^3}]{1025.9}$ \\ [1ex]
	$\mu$ &   $\np[Pa\cdot s]{0.01}$  \\ [1ex]
	$\eta$ & $\np[Pa]{0.1611}$ \\ [1ex]
	n & 0.5859 \\ [1ex]
	K & $\np[\frac{1}{s}]{58.0725}$  \\ [1ex]
	\hline
\end{tabular}
\caption{Parameter values for use in clot formulation.}
\label{tab:Parameters}
\end{table}
%

In Lagrangian coordinates, we have the following weak form:

%%
\begin{align}
	\label{eq:CLWF1}
	\int_{\Omega_{X}}(J-1)\tilde{p}~J\d{V}&=0,
	\\
	\label{eq:CLWF2}
	\int_{\Omega_{X}}\rho\frac{\partial^2\bv{u}}{\partial t^2}\cdot\tilde{\bv{u}}\, J\d{V}+\int_{\Omega_{X}} \tensor{T}\inversetranspose{\tensor{F}} \colon\nabla_{X}\tilde{\bv{u}}\, J\d{V}-\int_{\partial\Omega_{X}}J\tensor{T}\inversetranspose{F}\hat{\bv{n}}_{X}\cdot\tilde{\bv{u}}\d{A}&=0,
	\\
	\label{eq:CLWF3}
	\int_{\Omega_{X}}\biggl[\frac{\partial\tensor{B}}{\partial t}-(\nabla_{X}\bv{v})\tensor{F}^{-1}\tensor{B}-\tensor{B}\inversetranspose{\tensor{F}}\transpose{(\nabla_{X}\bv{v})}+2 \, \text{K} \, \bigl(\text{tr}(\tensor{B})-3\lambda \bigr)^{n} \bigl(\tensor{B}-\lambda\tensor{I} \bigr)\biggr]\colon\tilde{\tensor{B}}~ J\d{V}&=0,
\end{align}
%%
with
%%
\begin{equation}
	\label{eq:CLCauchy}
	\tensor{T}=-p\tensor{I}+\eta\Bigl[\bigl(\frac{\partial^2\bv{u}}{\partial t\partial{\bv{X}}}\bigr)~\tensor{F}^{-1}+\inversetranspose{\tensor{F}}\transpose{\bigl(\frac{\partial^2\bv{u}}{\partial t\partial{\bv{X}}}\bigr)}\Bigr]+\mu(\tensor{B}-\tensor{I}).
\end{equation}
%%

$\tensor{B}$, $\frac{\partial\tensor{B}}{\partial t}$, and $\lambda$ are given by Eqs.~\eqref{eq:CBDef}, \eqref{eq:CBtDef}, and \eqref{eq:lambdamod} respectively. Lastly, we have our ALE formulation:

%%
\begin{align}
	\label{eq:ALEWF1}
	\int_{\Omega_{\chi}}\trace(\nabla_{\chi}\bv{v}\hat{\tensor{F}}^{-1})\tilde{p}~\hat{J}\d{V}&=0,
	\\
	\int_{\Omega_{\chi}}\rho\biggl[\frac{\partial \bv{v}}{\partial t}+(\nabla_{\chi}\bv{v})\cdot\hat{\tensor{F}}^{-1}(\bv{v}-\frac{\partial\hat{\bv{u}}}{\partial t})\biggr]\cdot\tilde{\bv{v}}\, \hat{J}\d{V}+\int_{\Omega_{\chi}} \hat{J}\tensor{\hat{T}}\inversetranspose{\hat{\tensor{F}}} \colon\nabla_{\chi}\tilde{\bv{v}}\, \d{V} \nonumber
	\\
	\label{eq:ALEWF2}
	-\int_{\partial\Omega_{\chi}}\hat{J}\hat{\tensor{T}}\inversetranspose{\hat{\tensor{F}}}\hat{\bv{n}}_{\chi}\cdot\tilde{\bv{v}}\d{A}&=0,
	\\
	\int_{\Omega_{\chi}}\biggl[\frac{\partial\tensor{B}}{\partial t}-(\nabla_{\chi}\bv{v})\hat{\tensor{F}}^{-1}\tensor{B}-\tensor{B}\inversetranspose{\hat{\tensor{F}}}\transpose{(\nabla_{\chi}\bv{v})}+2 \, \text{K} \, \bigl(\text{tr}(\tensor{B})-3\lambda \bigr)^{n} \bigl(\tensor{B}-\lambda\tensor{I} \bigr)\biggr]\colon\tilde{\tensor{B}}~ \hat{J}\d{V} \nonumber
	\\
	-\int_{\Omega_{\chi}}\Bigl(\tensor{B}\otimes\bigl(\hat{\tensor{F}}^{-1}(\bv{v}-\frac{\partial\hat{\bv{u}}}{\partial t})\bigr)\Bigr)\colon\nabla{\tilde{\tensor{B}}}\, \hat{J}\d{V} \nonumber
	\\
	\label{eq:ALEWF3}
	+\int_{\partial\Omega_{\chi}}\Bigl(\tensor{B}\otimes\bigl(\hat{\tensor{F}}^{-1}(\bv{v}-\frac{\partial\hat{\bv{u}}}{\partial t})\bigr)\Bigr)\, \hat{\bv{n}}\colon\tilde{\tensor{B}}\, \hat{J} \d{A}&=0,
%%should there be a J at the end of the last integral??
	\\
	\label{eq:ALEWF4}
	\int_{\Omega_{\chi}}\nabla_{\chi}\hat{\bv{u}}\colon\nabla_{\chi}\tilde{\bv{u}}\, \d{V}+\int_{\partial\Omega_{\chi}}\biggl(\frac{1}{\gamma\text{h}}(\bv{v}-\frac{\partial\hat{\bv{u}}}{\partial t})-(\nabla_{\chi}\hat{\bv{u}})\hat{\bv{n}}\biggr)\cdot\tilde{\bv{u}}\, \d{A} &= 0
\end{align}
%%

where 

%%
\begin{equation}
	\label{eq:CauchyALE}
	\hat{\tensor{T}}=-p\tensor{I}+\eta\bigl((\nabla_{\chi}\bv{v})~\hat{\tensor{F}}^{-1}+\inversetranspose{\hat{\tensor{F}}}\transpose{(\nabla_{\chi}\bv{v})}\bigr)+\mu(\tensor{B}-\tensor{I}),
\end{equation}
%%

and Eqs.~\eqref{eq:CBDef}, \eqref{eq:CBtDef}, and \eqref{eq:lambdamod} hold. 

\section{Study Components}

From this point on, we are not concerned with solving a physical problem. We instead will perform the same verification of our three formulations using the Method of Manufactured Solutions as in Chapter~3 \cite{Salari2000}. We choose the same fields as in the Oldroyd-B problem for our unknown quantities:

%%
\begin{align}
	\label{eq:ClotMSp}
	\MS{MSp}(x,y,t)&=p_{0} \cos\bigl(2\pi\,\frac{t}{t_{0}}\bigr)\cos\bigl(2\pi\,\frac{x+y}{L}\bigr),
	\\
	\label{eq:ClotMSv}
	\bv{\MS{MSv}}(x,y,t)&=v_{0}\sin\bigl(2\pi\,\frac{t}{t_{0}}\bigr)\biggl[\sin\bigl(2\pi\,\frac{x+y}{L}\bigr) \, \ui + \cos\bigl(2\pi\,\frac{x-y}{L}\bigr) \, \uj \biggr],
	\\
	\label{eq:ClotMSu}
	\bv{\MS{MSu}}(x,y,t)&=u_{0}\sin\bigl(2\pi\,\frac{t}{t_{0}}\bigr)\biggl[\sin\bigl(2\pi\,\frac{x+y}{L}\bigr) \, \ui + \cos\bigl(2\pi\,\frac{x-y}{L}\bigr) \, \uj \biggr],
	\\
	\label{eq:ClotMSuhat}
	\bv{{\MS{MS\hat{u}}}}(x,y,t)&=\hat{u}_{0}\sin\bigl(2\pi\,\frac{t}{t_{0}}\bigr)\biggl[\sin\bigl(2\pi\,\frac{x+y}{L}\bigr) \, \ui + \cos\bigl(2\pi\,\frac{x-y}{L}\bigr) \, \uj \biggr],
	\\
	\label{eq:ClotMSv_ALE}
	\bv{\MS{MSv}}_{A}(x,y,t)&=\frac{2\pi \hat{u}_{0}}{t_{0}}\cos\bigl(2\pi\,\frac{t}{t_{0}}\bigr)\biggl[\sin\bigl(2\pi\,\frac{x+y}{L}\bigr) \, \ui + \cos\bigl(2\pi\,\frac{x-y}{L}\bigr) \, \uj \biggr]
\end{align}
%%

and \tensor{MSG} as in Eq.~\eqref{eq:MSG}. For the constants above, $p_0=\np[Pa]{1}$, $v_{0}=\np[m/s]{0.01}$, $u_{0}=\hat{u}_{0}=\np[m]{0.01}$, $G_0=1$, $L=\np[m]{1}$, and $t_0=\np[s]{1}$. These functions at time $t=0.7$ are depicted in Figs.~\ref{fig:ClotMSp}-\ref{fig:ClotMSG11}.

%
\begin{figure}[h!]
\centering
\includegraphics[scale=0.4]{Chapter-5/Figures/MSp_7}
\caption{Plot of $\MS{MSp}$ at time $t=\np[s]{0.7}$.}
\label{fig:ClotMSp}
\end{figure}
%
%
\begin{figure}[h!]
\centering
\includegraphics[scale=0.4]{Chapter-5/Figures/MSv1_7}
\caption{Plot of the $\ui$-component of $\bv{\MS{MSv}}$, $\bv{\MS{MSu}}$, and $\bv{\MS{MS\hat{u}}}$ at time $t=\np[s]{0.7}$.}
\label{fig:ClotMSv1}
\end{figure}
%
%
\begin{figure}[h!]
\centering
\includegraphics[scale=0.55]{Chapter-5/Figures/MSv1ALE_7} 
\caption{Plot of the $\ui$-component of the $\bv{\MS{MSv}}_{A}$ at time $t=\np[s]{0.7}$.}
\label{fig:ClotMSv1ALE}
\end{figure}
%
%
\begin{figure}[h!]
\centering
\includegraphics[scale=0.4]{Chapter-5/Figures/MSG11_7} 
\caption{$\ui \otimes \ui$ component of $\tensor{\MS{MSG}}$ at time $t=\np[s]{0.7}$.}
\label{fig:ClotMSG11}
\end{figure}
%

It is at this point that we work backwards from the solutions to obtain the source terms, boundary conditions, and initial conditions for each field. As with the Oldroyd-B case study, we will impose pure Dirichlet boundary conditions on $\bv{v}$ and $\bv{u}$ and a constraint on $p$, which states that its global average is zero. These calculations are carried out in \Mathematica, outputted to a text file, then imported directly into \COMSOL.

We solve our system in two-dimensions over a square domain of side-length $L=\np[m]{1}$. The mesh is comprised of mapped quadrilateral elements of side length h. We solve the system six times, each time refining the mesh by halving the length of h. Each field is evaluated exactly at the nodes of our mesh and the solution is interpolated between these points by a linear combination of polynomial shape functions. We select Lagrange quadratic shape functions for the vector-valued fields: $\bv{v}$, $\bv{u}$, $\bv{\hat{u}}$. For $p$ and $\tensor{G}$, we choose Lagrange linear elements. As our system is time-dependent, we use a variable-order BDF to integrate over time, which is an IDA solver for differential-algebraic-equations \cite{Hindmarsh2005}. In the next chapter, we will present the numerical solutions of this problem, and provide an analysis on the error incurred by the solver.

