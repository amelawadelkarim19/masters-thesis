%!TEX root = ../YourName-Dissertation.tex
\chapter{Oldroyd-B Results \& Analysis}

For each formulation of the problem, we have performed our verification analysis with five uniform mesh refinements, though only the first, third, and last refinements are shown to illustrate the resulting smoothness. The plots in this Chapter are of raw \COMSOL results, with no optical refinement on the resolution. The original form of our balance laws are Eulerian, so we begin the presentation of results with this trial. 

\section{Eulerian Results}

Recall the plots of our manufactured solution for $p$, $\bv{v}$, and $\tensor{G}$ in Figs.~\ref{fig:MSp}, \ref{fig:MSv1}, and \ref{fig:MSG11} respectively. The corresponding \COMSOL outputs of these fields, with various mesh lengths can be seen in Figs.~\ref{fig:Eulerp}--\ref{fig:EulerG}, all at time $t=\np[s]{0.7}$. 

%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.42]{Chapter-4/Figures/Eulerian_p_1} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/Eulerian_p_3} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/Eulerian_p_5}
\caption{Numerical solution of $p$ in Eulerian framework.
\\
Left to Right: $h=\np[m]{0.25}$, $h=\np[m]{0.0625}$, and $h=\np[m]{1.563e-2}$.}
\label{fig:Eulerp}
\end{figure}
%%
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.42]{Chapter-4/Figures/Eulerian_v_1} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/Eulerian_v_3} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/Eulerian_v_5}
\caption{Numerical solution of $\bv{v}_1$ in Eulerian framework.
\\
Left to Right: $h=\np[m]{0.25}$, $h=\np[m]{0.0625}$, and $h=\np[m]{1.563e-2}$.}
\label{fig:Eulerv}
\end{figure}
%%
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.452]{Chapter-4/Figures/Eulerian_G_1} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/Eulerian_G_3} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/Eulerian_G_5}
\caption{Numerical solution of $\tensor{G}_{11}$ in Eulerian framework.
\\
Left to Right: $h=\np[m]{0.25}$, $h=\np[m]{0.0625}$, and $h=\np[m]{1.563e-2}$.}
\label{fig:EulerG}
\end{figure}
%%

Since we know the exact solutions, we can calculate the $L^2$ and $H^1$ norms of the error between the two at various characteristic mesh lengths, $h$. The magnitude of these errors are tabulated in Table~\ref{tab:EulerError1}. Plotting the log of these norms versus the log of the inverse of the mesh length, we obtain Fig.~\ref{fig:EulerConv}. All mesh lengths, $h$, are given in meters.

%%
\begin{table}[h!]
\begin{center}
\renewcommand{\arraystretch}{1.25}
\begin{tabular}{c c c c c c} 
\hline
$h$ [m] & \# d.o.f. & $\| \MS{MSp}-$p$ \|_{L^2}$ & $\| \bv{\MS{MSv}}-\bv{v} \|_{L^2}$ & $\| \bv{\MS{MSv}}-\bv{v} \|_{H^1}$ & $\| \MS{\tensor{MSG}}-\tensor{G} \|_{L^2}$ \\
\hline
\np{0.25} & 262  & $\np{4.882e-4}$     & $\np{4.907e-8}$ & $\np{6.887e-5}$ & $\np{3.273e-5}$ \\
\np{0.125} & 902  & $\np{1.323e-5}$    & $\np{1.122e-10}$ & $\np{1.232e-6}$ & $\np{9.337e-7}$ \\
\np{0.0625} & 3334  & $\np{4.839e-7}$     & $\np{4.836e-13}$ & $\np{3.033e-8}$ & $\np{3.472e-8}$ \\ 
\np{3.125e-2} & 12806 & $\np{1.928e-8}$   & $\np{3.398e-15}$ & $\np{1.036e-9}$ & $\np{1.390e-9}$ \\ 
\np{1.563e-2} & 50182 & $\np{7.861e-10}$     & $\np{3.131e-17}$ & $\np{4.049e-11}$ & $\np{5.712e-11}$ \\ 
\hline
\end{tabular}
\end{center}
\caption{Error in numerical solution of $p$, \bv{v}, and \tensor{G} in Eulerian framework.}
\label{tab:EulerError1}
\end{table}
%%

%%
\begin{figure}[h!]
\centering
\includegraphics[scale=0.6]{Chapter-4/Figures/Eulerian_Conv}
\caption{Plot of the $L^{2}$ and $H^1$ norms of error for $p$, $\bv{v}$, and $\tensor{G}$ -- Eulerian.}
\label{fig:EulerConv}
\end{figure}
%%

\section{Lagrangian Results}
Recall the plots of our manufactured solution for $p$, $\bv{u}$, and $\tensor{G}$ in Figs.~\ref{fig:MSp}, \ref{fig:MSv1}, and \ref{fig:MSG11} respectively. The corresponding \COMSOL outputs of these fields, with various mesh lengths can be seen in Figs.~\ref{fig:Lagp}--\ref{fig:LagG}, all at time $t=\np[s]{0.7}$. 
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.42]{Chapter-4/Figures/Lagrangian_p_1} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/Lagrangian_p_3} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/Lagrangian_p_5}
\caption{Numerical solution of $p$ in Lagrangian framework.
\\
Left to Right: $h=\np[m]{0.25}$, $h=\np[m]{0.0625}$, and $h=\np[m]{1.563e-2}$.}
\label{fig:Lagp}
\end{figure}
%%
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.42]{Chapter-4/Figures/Lagrangian_u_1} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/Lagrangian_u_3} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/Lagrangian_u_5}
\caption{Numerical solution of $\bv{u}_1$ in Lagrangian framework.
\\
Left to Right: $h=\np[m]{0.25}$, $h=\np[m]{0.0625}$, and $h=\np[m]{1.563e-2}$.}
\label{fig:Lagu}
\end{figure}
%%
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.42]{Chapter-4/Figures/Lagrangian_G_1} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/Lagrangian_G_3} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/Lagrangian_G_5}
\caption{Numerical solution of $\tensor{G}_{11}$ in Lagrangian framework.
\\
Left to Right: $h=\np[m]{0.25}$, $h=\np[m]{0.0625}$, and $h=\np[m]{1.563e-2}$.}
\label{fig:LagG}
\end{figure}
%%

The magnitude of the $L^2$ and $H^1$ errors incurred in this formulation are tabulated in Table~\ref{tab:LagError1}. Plotting the log of these norms versus the log of the inverse of the mesh length, we obtain Fig.~\ref{fig:LagConv}. Again, all mesh lengths, $h$, are given in meters.

%%
\begin{table}[h!]
\begin{center}
\renewcommand{\arraystretch}{1.25}
\begin{tabular}{c c c c c c} 
\hline
$h$ [m] & \# d.o.f. & $\| \MS{MSp}-$p$ \|_{L^2}$ & $\| \bv{\MS{MSu}}-\bv{u} \|_{L^2}$ & $\| \bv{\MS{MSu}}-\bv{u} \|_{H^1}$ & $\| \MS{\tensor{MSG}}-\tensor{G} \|_{L^2}$ \\
\hline
\np{0.25} & 262  & $\np{5.173e-4}$     & $\np{1.245e-8}$ & $\np{2.228e-5}$ & $\np{3.501e-5}$ \\
\np{0.125} & 902  & $\np{1.328e-5}$    & $\np{5.528e-11}$ & $\np{6.606e-7}$ & $\np{9.387e-7}$ \\
\np{0.0625} & 3334  & $\np{4.841e-7}$     & $\np{3.797e-13}$ & $\np{2.439e-8}$ & $\np{3.458e-8}$ \\ 
\np{3.125e-2} & 12806 & $\np{1.928e-8}$   & $\np{3.006e-15}$ & $\np{9.732e-10}$ & $\np{1.382e-9}$ \\ 
\np{1.563e-2} & 50182 & $\np{7.876e-10}$     & $\np{2.480e-17}$ & $\np{3.968e-11}$ & $\np{5.640e-11}$ \\ 
\hline
\end{tabular}
\end{center}
\caption{Error in numerical solution of $p$, \bv{u}, and \tensor{G} in Lagrangian framework.}
\label{tab:LagError1}
\end{table}
%%

%%
\begin{figure}[h!]
\centering
\includegraphics[scale=0.6]{Chapter-4/Figures/Lagrangian_Conv}
\caption{Plot of the $L^{2}$ and $H^1$ norms of error for $p$, $\bv{u}$, and $\tensor{G}$ -- Lagrangian.}
\label{fig:LagConv}
\end{figure}
%%

\section{ALE Results}
Recall the plots of our manufactured solution for $p$, $\bv{v}$, $\hat{\bv{u}}$ and $\tensor{G}$ in Figs.~\ref{fig:MSp}, \ref{fig:MSv1ALE}, \ref{fig:MSv1}, and \ref{fig:MSG11} respectively. The corresponding \COMSOL outputs of these fields, with various mesh lengths can be seen in Figs.~\ref{fig:ALEp}--\ref{fig:ALEu}, all at time $t=\np[s]{0.7}$. 
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.42]{Chapter-4/Figures/ALE_p_1} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/ALE_p_3} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/ALE_p_5}
\caption{Numerical solution of $p$ in ALE framework.
\\
Left to Right: $h=\np[m]{0.25}$, $h=\np[m]{0.0625}$, and $h=\np[m]{1.563e-2}$.}
\label{fig:ALEp}
\end{figure}
%%
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.42]{Chapter-4/Figures/ALE_v_1} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/ALE_v_3} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/ALE_v_5}
\caption{Numerical solution of $\bv{v}$ in ALE framework.
\\
Left to Right: $h=\np[m]{0.25}$, $h=\np[m]{0.0625}$, and $h=\np[m]{1.563e-2}$.}
\label{fig:ALEv}
\end{figure}
%%
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.42]{Chapter-4/Figures/ALE_G_1} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/ALE_G_3} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/ALE_G_5}
\caption{Numerical solution of $\tensor{G}$ in ALE framework.
\\
Left to Right: $h=\np[m]{0.25}$, $h=\np[m]{0.0625}$, and $h=\np[m]{1.563e-2}$.}
\label{fig:ALEG}
\end{figure}
%%
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.42]{Chapter-4/Figures/ALE_u_1} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/ALE_u_3} \hfill
\includegraphics[scale=0.42]{Chapter-4/Figures/ALE_u_5}
\caption{Numerical solution of $\hat{\bv{u}}$ in ALE framework.
\\
Left to Right: $h=\np[m]{0.25}$, $h=\np[m]{0.0625}$, and $h=\np[m]{1.563e-2}$.}
\label{fig:ALEu}
\end{figure}
%%

The magnitude of the $L^2$ and $H^1$ errors incurred in this formulation are tabulated in Table~\ref{tab:ALEError1} and Table~\ref{tab:ALEError2}. Plotting the log of these norms versus the log of the inverse of the mesh length, we obtain Fig.~\ref{fig:ALEConv}. All mesh lengths, $h$, are given in meters.

%%
\begin{table}[h!]
\begin{center}
\renewcommand{\arraystretch}{1.25}
\begin{tabular}{c c c c c c} 
\hline
$h$ [m] & \# d.o.f. & $\| \MS{MSp}-$p$ \|_{L^2}$ & $\| \bv{\MS{MSv}}-\bv{v} \|_{L^2}$ & $\| \bv{\MS{MSv}}-\bv{v} \|_{H^1}$ & $\| \MS{\tensor{MSG}}-\tensor{G} \|_{L^2}$ \\
\hline
\np{0.25} & 592  & $\np{5.045e-4}$     & $\np{4.628e-8}$ & $\np{1.052e-4}$ & $\np{1.404e-4}$ \\
\np{0.125} & 2104  & $\np{1.326e-5}$    & $\np{2.465e-10}$ & $\np{3.424e-6}$ & $\np{7.709e-6}$ \\
\np{0.0625} & 7912  & $\np{4.837e-7}$     & $\np{1.876e-12}$ & $\np{1.271e-7}$ & $\np{3.666e-7}$ \\ 
\np{3.125e-2} & 30664 & $\np{1.927e-8}$   & $\np{1.537e-14}$ & $\np{5.047e-9}$ & $\np{1.576e-8}$ \\ 
\np{1.563e-2} & 120712 & $\np{7.855e-10}$     & $\np{1.275e-16}$ & $\np{2.054e-10}$ & $\np{6.556e-10}$ \\ 
\hline
\end{tabular}
\end{center}
\caption{Error in numerical solution of $p$, \bv{v}, and \tensor{G} in ALE framework.}
\label{tab:ALEError1}
\end{table}
%%
%%
\begin{table}[h!]
\begin{center}
\renewcommand{\arraystretch}{1.25}
\begin{tabular}{c c c c} 
\hline
$h$ & \# d.o.f. & $\| \MS{\bv{MS\hat{u}}}-\hat{\bv{u}} \|_{L^2}$ & $\| \MS{\bv{MS\hat{u}}}-\hat{\bv{u}} \|_{H^1}$  \\ \hline
\np{0.25} & 592  & $\np{4.473e-9}$  & $\np{1.344e-5}$ \\
\np{0.125} & 2104  & $\np{4.124e-11}$ & $\np{5.669e-7}$ \\
\np{0.0625} & 7912  & $\np{3.520e-13}$ & $\np{2.341e-8}$ \\ 
\np{3.125e-2} & 30664 & $\np{2.950e-15}$ & $\np{9.629e-10}$ \\ 
\np{1.563e-2} & 120712 & $\np{2.462e-17}$  & $\np{3.958e-11}$ \\ \hline
\end{tabular}
\end{center}
\caption{Error in numerical solution of \bv{\hat{u}} in ALE framework.}
\label{tab:ALEError2}
\end{table}
%%

%%
\begin{figure}[h!]
\centering
\includegraphics[scale=0.6]{Chapter-4/Figures/ALE_Conv}
\caption{Plot of the $L^{2}$ and $H^1$ norms of error for $p$, $\bv{v}$, $\hat{\bv{u}}$, and $\tensor{G}$ -- ALE.}
\label{fig:ALEConv}
\end{figure}
%%

\section{Analysis}

In all three formulations of our system, we can qualitatively observe that refining the mesh allows the interpolation to recover the true solution with increased accuracy. For each field, the course mesh results illustrate that the linear or quadratic interpolation between nodes falls short at capturing the sinusoidal nature of our solutions. However, by the fifth mesh refinement, the solution for each field looks nearly exact for all three formulations. Quantitatively, each field converges uniformly to the exact solution with further refinement of the mesh, as evidenced in Figs.~\ref{fig:EulerConv}, \ref{fig:LagConv}, and \ref{fig:ALEConv}. The solutions for $\bv{v}$, $\bv{u}$, and $\hat{\bv{u}}$ show the least error by several orders of magnitude, essentially recovering the true solution with only machine precision error by the last mesh refinement. The solutions converge at a rate of 3 and their gradients converge at a rate of 2. The solutions for $p$ and $\tensor{G}$, while less accurate, still exhibit a uniform convergence upon mesh refinement at a rate of 2. If computational cost were no issue, the exact solution can be recovered for all fields for each formulation.

It is important to note that this study is not meant to compare the benefits of each formulation type. If so, the conclusion would be that the three are interchangeable, which is a fallacy. We have simply performed a verification analysis via the Method of Manufactured solutions to show that we have solved these problems \emph{correctly}. Once we apply these solvers to our physical system with the proper boundary conditions, constraints, and source fields, deciding which formulation to employ may be the difference between obtaining our desired results and being unable to solve the system entirely. This phase of research is beyond the scope of this study, but will be explored in the future.



