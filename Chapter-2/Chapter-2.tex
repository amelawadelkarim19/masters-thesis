%!TEX root = ../YourName-Dissertation.tex
\chapter{Fluid Models \& Oldroyd-B Example}

In this Chapter, we will first discuss and derive some existing fluid models, introducing the concept of the evolving natural reference configuration of a body, as in \cite{Rajagopal2000}. Then, we will formulate a two-dimensional, initial boundary value problem for one such fluid, namely the Oldroyd-B model. We will describe the same example problem in three forms: Eulerian, Lagrangian, and ALE to illustrate their differences and highlight the benefits of each. This chapter will serve as the framework for modeling clot behavior using the rheological clot model found in \cite{Anand2004Visco}.

\section{Existing Fluid Models}

Though blood is a common fluid to every living mammal, its rheological characteristics are amazingly complex and highly variable due to the effects of its environment, flow velocity, and any pathological abnormalities. Depending on the application, a wide variety of fluid models may satisfy the criteria of the situation, some even contradictory.

\subsection{Power Law Model}

There are four common categories of fluids --- Newtonian, Dilatant, Pseudoplastic, and Bingham Plastic --- each with its own flow characteristics. A Newtonian fluid is defined as a material whose stress is linearly proportional to its strain rate. This can be expressed mathematically using the following relation,
%%
\begin{equation}
	\label{eq:Newtonian}
	\tau=\eta \nabla v,
\end{equation}
%%

where $\tau$ is fluid shear stress, $\eta$ is the constant of proportionality or dynamic viscosity of the fluid, $v$ is the fluid velocity, and $\nabla v$ is the strain rate, i.e., the gradient of the velocity.

Many fluids we deal with everyday exhibit this simplistic relationship under ordinary conditions and are classified as Newtonian fluids, such as water or air. The three other categories of fluids, however, showcase more interesting attributes. Dilatant, also known as shear-thickening, fluids become more stiff when they are abruptly strained, while pseudoplastic, or shear-thinning, fluids become less viscous with faster strain rates. An example of each would be quicksand and toothpaste respectively. The Power Law Model effectively depicts the differences between the first three models discussed thus far:

%%
\begin{equation}
	\label{eq:PowerLawModel}
	\tau=\eta(\nabla v)^{n}
\end{equation}
%%
When $n=1$, this relationship recovers the equation for a Newtonian fluid. For $n\geq1$, Eq.~\eqref{eq:PowerLawModel} describes the behavior of a dilatant fluid, whereas for $0 < n < 1$, it describes a pseudoplastic fluid. 

Bingham plastic fluids demonstrate distinct behaviors compared to the last three --- a certain yield stress must be first overcome before the fluid behaves like a Newtonian fluid. Denoting the threshold stress necessary for flow to occur by $\tau_{y}$, then the shear stress of the material is modeled by a relation of the following type:
%%
\begin{equation}
	\label{eq:BP}
	\tau=\eta\nabla v + \tau_\text{y}.
\end{equation}
%%

As mentioned before, there is a vast literature on the modeling of blood. For example, blood is widely recognized to behave as a non-Newtonian fluid, in fact it exhibits clear shear-thinning behavior \cite{Tazraei2015}, but one can make the argument that it can be modeled as a Newtonian fluid in some cases, e.g. if the blood constituents are sufficiently small compared to the diameter of the blood vessel.

\subsection{Maxwell-Type Model}

The four basic fluid models discussed above are one-dimensional rheological models convenient when trying to conceptually illustrate basic physical deformation mechanisms.  In addition, said models do not contain all of the features needed to model blood behavior under a wide range of flow regimes or while undergoing clot formation or lysis.  The three-dimensional generalization of these models is typically carried out with the use of continuum mechanics and thermodynamics. The models to be discussed in this section all behave as viscoelastic fluids, or fluids that display a mixture of both viscous and elastic responses to deformation, and are derived from a thermodynamic basis.

In the groundbreaking paper written by K.~R.~Rajagopal and A. R. Srinivasa in 2000 \cite{Rajagopal2000}, these authors derive several models for fluids with varying energy storage functions, $W$, and rate-of-dissipation functions, $\xi$ \cite{Rajagopal2000}. The work in \cite{Rajagopal2000}, based on the conceptual framework first proposed by J.~G.~Oldroyd in 1950 \cite{Oldroyd1949}, served as the basis for over 200 studies since its publication, proving its vital contribution to the field. 

%changed from "natural reference configuration"....isn't this a combination of distinct configs?
The starting point for the construction of the model in \cite{Rajagopal2000} is that of \emph{natural reference configuration} of a body.  In Continuum Mechanics, a reference configuration is said to be \emph{natural} when the free energy of the material achieves a minimum \cite{GurtinFried_2010_The-Mechanics_0}. This definition is then shown to imply that the material in a natural reference configuration is stress free, or completely relaxed.  When applied to a viscoelastic material, we then have that a natural reference configuration in one in which the fluid can be at rest, i.e., does not flow. With this premise, Rajagopal and co-workers have proposed that the rheological response of complex viscoelastic materials to applied loadings indicates the presence of molecular rearrangements that cause the stress-free configuration of the material to \emph{evolve} as function of the loading history.

Along with a unique thermodynamic perspective, Rajagopal and co-workers then posit that complex viscoelastic fluids should be modeled as materials with \emph{evolving natural configurations}.  It is this concept that sets the work in \cite{Rajagopal2000} apart from those that had been previously derived.  Borrowing the nomenclature used in \cite{Rajagopal2000}, we define $\kappa_R$ as the reference configuration of a body of fluid and $\kappa_t$ as its current configuration. The natural configuration of the fluid, $\kappa_{p(t)}$, results from the relaxation from the current configuration. Following \cite{Rajagopal2000}, a schemating illustrating these three configurations is given in Fig.~\ref{fig:Rajagopal2000}.

%%
\begin{figure}[h]
\centering
\includegraphics[scale=0.75]{Chapter-2/Figures/EvolvingRefDrawing}
\caption{Schematic of the evolving natural configuration. The tensor $\tensor{F}_{\kappa_{R}}$ is the deformation gradient of the line elements surrounding a material point $\bv{X}$ in the reference to those in the current configuration. Similarly, $\tensor{F}_{\kappa_{p(t)}}$ is the deformation of the line elements in the natural configuration to the current one. The solid line from $\kappa_r$ to $\kappa_t$, $S_{\kappa_{R}}(\bv{X},t)$ is merely the motion of a material point, $\bv{X}$, through time.}
\label{fig:Rajagopal2000}
\end{figure}
%%

For a purely elastic fluid which reverts back to its initial state when alleviated of stress, there is only one natural configuration, namely the reference configuration of the body. On the other hand, for a purely plastic body which permanently deforms as stress is applied, there are infinitely many natural configurations, specifically being the evolving current configuration itself. 

Many fluids we deal with in mathematical problems, including blood, do not simply fall into one of these two categories, but rather illustrate varying degrees of both. If the material response is not purely elastic or purely plastic, how can we deduce which natural configuration the current body would relax upon? The method used by Rajagopal and Srinivasa is based on the general principle that the body chooses to relax to a stress-free state through a deformation process for which \textit{the rate of dissipation of the material is maximized.} The rationale for this choice goes beyond the scope of this project, but an example derivation of a fluid model as a consequence of this principle can be found in \cite{Rajagopal2000}.

Among those derived in \cite{Rajagopal2000}, a Maxwell type fluid model is obtained through a distinct equation for the rate of dissipation for the fluid. In this case, the constitutive equation blends the rate of dissipation of a viscous Newtonian fluid with an assumption on the stored energy that resembles a neo-Hookean solid, the latter being a hyperelastic material \cite{Rajagopal2000}.

The scalar dissipation rate and stored energy functions are given in the following form:
%%
\begin{align}
	\label{eq:MaxStoredEnergy}
	\hat{W}&=\frac{\mu}{2}(I-3),
	\\
	\label{eq:MaxRateDissip}
	\hat{\xi} &=\eta \, \tensor{D}_{\kappa_{p(t)}} \cdot \tensor{B}_{\kappa_{p(t)}} \tensor{D}_{\kappa_{p(t)}}
\end{align}
%%
where $\mu$ is the elastic shear modulus, $\eta$ is the dynamic viscosity, $\tensor{D}_{\kappa_{p(t)}}$ represents the rate of change of the natural configuration of the body, $\tensor{B}_{\kappa_{p(t)}}$ is the left Cauchy-Green stress tensor relative to the current natural configuration, and $I=\trace(\tensor{B}_{\kappa_{p(t)}})$, i.e., $I$ is the first principal invariant of $\tensor{B}_{\kappa_{p(t)}}$. 

%It is important to note that we have changed parameter names; while $\mu$ was the dynamic viscosity in the fluid models discussed earlier in the chapter, $\eta$ is now the viscosity and $\mu$ is the elastic modulus of the material.

Applying these constitutive assumptions to the modeling of an incompressible Maxwell type fluid model, and following the procedure outlined in \cite{Rajagopal2000}, one obtains
%%
\begin{align}
	\label{eq:MaxStress}
	\tensor{T}&=-p\tensor{I}+\tensor{S},
	\\
	\label{eq:MaxS}
	\tensor{S}&=\mu\tensor{B}_{\kappa_{p(t)}},
	\\
	\label{eq:MaxBDeriv}
	-\tfrac{1}{2}\UCTD{\tensor{B}}_{\kappa_{p(t)}}&=\frac{\mu}{\eta}\bigl(\tensor{B}_{\kappa_{p(t)}}-\lambda\tensor{I}\bigr),
\end{align}
%%
where $\UCTD{\tensor{B}}_{\kappa_{p(t)}}$, denotes the \emph{upper convected time derivative} of $\tensor{B}_{\kappa_{p(t)}}$. Denoting by $\tensor{L}$ the velocity gradient of the body, i.e., $\tensor{L} = \nabla\bv{v}$, then the upper-convected time derivative of any tensor $\tensor{A}$ is defined as
%%
\begin{equation}
	\label{eq:UCDeriv}
	\UCTD{\tensor{A}}=\dot{\tensor{A}}-\tensor{L}\tensor{A}-\tensor{A}\transpose{\tensor{L}}.
\end{equation}
%%
Going back to Eqs.~\eqref{eq:MaxStress}--\eqref{eq:MaxBDeriv}, $\tensor{T}$ is the Cauchy, or true stress, $p$ is a scalar multiplier responsible for the enforcement of the incompressibility condition, and $\lambda$ is defined as:
%%
\begin{equation}
	\label{eq:MaxLambda}
	\lambda=\frac{3}{\trace\tensor{B}^{-1}_{\kappa_{p(t)}}}.
\end{equation}
%%
The multiplier $p$ is often erroneously referred to as the fluid pressure.  In reality, $p$ does not represent a physical pressure experienced by the fluid, rather it takes on the units of a pressure and has an effect on the stress of the system. One should note that certain rearrangements and simplifications of these equations results in the traditional upper-convected Maxwell model \cite{Rajagopal2000}.

\subsection{Oldroyd-B Model}

Starting again from \cite{Rajagopal2000}, we explore another equation for the rate of dissipation, namely,

%%
\begin{equation}
	\label{eq:OldRateDissip}
	\hat{\xi}=\eta \tensor{D}_{\kappa_{p(t)}} \cdot \tensor{B}_{\kappa_{p(t)}} \tensor{D}_{\kappa_{p(t)}}+\eta_{1}\tensor{D} \cdot \tensor{D},
\end{equation}
%%
where
%%
\begin{equation}
	\label{eq:SymL}
	\tensor{D}=\sym(\tensor{L}) = \tfrac{1}{2}\bigl[\nabla\bv{v}+\transpose{(\nabla\bv{v})}\bigr].
\end{equation}
%%
The difference between this assumption and the one made for the Maxwell model is the extra dissipative term, specifically the quadratic relationship to the velocity gradient of the physical body, exhibiting further Newtonian dissipative behavior \cite{Rajagopal2000}. The term also introduces a second viscosity of the material, $\eta_{1}$, which is the viscosity of the motion of the body and the viscosity that we are more familiar with. This variation on the dissipative equation, with the same stored energy function as with the Maxwell type model, results in the following set of constitutive equations:
%why does a quadratic velocity dependence signal newtonian behavior and not dilatant?
%%
\begin{align}
	\label{eq:OldStress}
	\tensor{T}&=-p\tensor{I}+\tensor{S},
	\\
	\label{eq:OldS}
	\tensor{S}&=\mu\tensor{B}_{\kappa_{p(t)}}+\eta_{1}\tensor{D},
	\\
	\label{eq:OldSDeriv}
	\tensor{S}+\frac{\eta}{2\mu}\UCTD{\tensor{S}}&=\eta_{1}\biggl(\tensor{D}+\frac{\eta}{2\mu}\UCTD{\tensor{D}}\biggr)+\mu\lambda\tensor{I}.
\end{align}
%%
This set of equations defines an Oldroyd-B fluid. All of the tensor and scalar quantities here match those discussed in the previous section.

One can express the above fluid model in the form used by Hron et al. in 2014 \cite{Hron2014}. The full proof of equivalence can be found in Appendix~A. The Oldroyd-B model used in \cite{Hron2014} is
%%
\begin{align}
	\label{eq:HronStress}
	\tensor{T}&=-p\tensor{I}+2\eta\tensor{D}+\mu(\tensor{B}-\tensor{I}),
	\\
	\label{eq:HronOldDeriv}
	\UCTD{\tensor{B}}&=\frac{1}{\tau}(\tensor{I}-\tensor{B}),
\end{align}
%%
where $\tau$ is the relaxation time of the material, and the upper-convected derivative of $\tensor{B}$ can be simplified from Eq.~\eqref{eq:UCDeriv} to 

%%
\begin{equation}
	\label{eq:HronUCDeriv}
	\UCTD{\tensor{B}}=\frac{\partial\tensor{B}}{\partial t}+(\nabla\tensor{B})\bv{v}-(\nabla	\bv{v})\tensor{B}-\tensor{B}\transpose{(\nabla\bv{v})}
\end{equation}
%%
One should note that we have dropped the subscript on $\tensor{B}$, but they are all understood to be $\tensor{B}_{\kappa_{p(t)}}$, the left Cauchy-Green stress tensor of the deformation mapping the current natural configuration to the current configuration. This notation will continue through the rest of this study.


\section{Oldroyd-B Problem Setup}

The example problem we wish to solve involves an incompressible Oldroyd-B fluid with the evolving natural configuration as declared in Eq.~\eqref{eq:HronOldDeriv}. This model is similar in form to the blood clot model used later, and was tested in \cite{Hron2014}, providing a guide to our formulations. Any fluid motion must conform to the balance of mass and of momentum. As the material has been assumed to be incompressible, the balance of mass reduces to a requirement that the material velocity field be divergence free (see, e.g., \cite{GurtinFried_2010_The-Mechanics_0}). Hence, we have the following governing equations comprising the \emph{strong form} of our problem: incompressibility (Eq.~\eqref{eq:Incompressibility}), balance of linear momentum (Eq.~\eqref{eq:BLM}), and the evolution of the natural configuration (Eq.~\eqref{eq:HronEvolNatConfig}):
%%
\begin{align}
	\label{eq:Incompressibility}
	\ldiv(\bv{v})=\trace(\nabla\bv{v})&=0,
	\\
	\label{eq:BLM}
	\rho\biggl[\frac{\partial\bv{v}}{\partial t}+(\nabla\bv{v})\bv{v}\biggr]&=\ldiv(\tensor{T}),
	\\
	\label{eq:HronEvolNatConfig}
	\frac{\partial\tensor{B}}{\partial t}+(\nabla\tensor{B})\bv{v}-(\nabla\bv{v})\tensor{B}-\tensor{B}\transpose{(\nabla\bv{v})}&=\frac{1}{\tau}(\tensor{I}-\tensor{B}),
\end{align}
%%
with $\tensor{T}$ as defined in Eq.~\eqref{eq:HronStress}. The unknown quantities in the above model are the scalar multiplier $p$ in Eq.~\eqref{eq:HronStress}, the vector velocity $\bv{v}$, and the strain tensor $\tensor{B}$, and the governing equations are provided in their Eulerian forms, the definition of which can be found in the following section.

Next, we reformulate this problem into the so-called \emph{weak form}.  This formulation is essential for the use of the Finite Element Method (FEM) in the numerical solution of the problem. The idea here is to transform the differential equations of the strong form, denoted $(S)$, into an equivalent set of integral equations, which are simpler to solve numerically, and reduce the order of differentiation required to obtain the solution. This is done by multiplying each equation by a set of ``test'' functions, integrating the new formula over the domain, then using integration-by-parts to reduce the highest order of differentiation under the integral.  The result is a set of integrals which are equivalent to the initial differential statement, and this is called the weak form of the problem, or $(W)$.

Multiplying both sides of our equations in $(S)$ by their corresponding test functions, then integrating over the current domain, $\Omega_{x}$, we obtain the weak form as in \cite{Hron2014}:
%%
\begin{align}
	\label{eq:WFIncompressibility}
	\int_{\Omega_{x}}\trace(\nabla\bv{v})\tilde{p} \, \d{v}&=0,
	\\
	\label{eq:WFBLM}
	\int_{\Omega_{x}}\rho\biggl[\frac{\partial\bv{v}}{\partial t}+(\nabla\bv{v})\bv{v}\biggr]\cdot\tilde{\bv{v}}\,\d{v}-\int_{\Omega_{x}}\ldiv(\tensor{T})\cdot\tilde{\bv{v}}\, \d{v}&=0,
	\\
	\label{eq:WFEvolNatConfig}
	\int_{\Omega_{x}}\biggl[\frac{\partial\tensor{B}}{\partial t}+(\nabla\tensor{B})\bv{v}-(\nabla\bv{v})\tensor{B}-\tensor{B}\transpose{(\nabla\bv{v})}+\frac{1}{\tau}(\tensor{B}-\tensor{I})\biggr]\cdot\tilde{\tensor{B}}\, \d{v}&=0,
\end{align}
%%
with the Cauchy Stress, $\tensor{T}$, written as
%%
\begin{equation}
	\label{eq:WFStress}
	\tensor{T}=-p\tensor{I}+\eta\bigl(\nabla\bv{v}+\transpose{(\nabla\bv{v})}\bigr)+\mu(\tensor{B}-\tensor{I}).
\end{equation}
%%
$\tilde{p}$, $\tilde{\bv{v}}$, and $\tilde{\tensor{B}}$ are the test functions for $p$, $\bv{v}$, and $\tensor{B}$ respectively. Again, these weak contributions are Eulerian and come from the expressions for the incompressibility, balance of linear momentum, and evolution of the natural configuration, as defined in Eqs.~\eqref{eq:Incompressibility}--\eqref{eq:HronEvolNatConfig}.

As the body is assumed to be incompressible, its motion is \emph{isochoric}, or volume-preserving. This assumption demands the determinant of $\tensor{F}$, and subsequently of $\tensor{B}$, to be equal to 1. We ensure this fact by defining $\tensor{B}$ in the following way, enhancing the model in \cite{Hron2014}:

%%
\begin{equation}
	\label{eq:BreDef}
	\tensor{B}=J_{G}^{-1/3}\tensor{G}.
\end{equation}
%%

where $\tensor{G}$ is an arbitrary tensor and $J_{G}$ is its determinant. $\tensor{G}$ has taken the place of $\tensor{B}$ as our principal unknown. This way, $\tensor{B}$ is defined with a determinant equal to 1. Note that this definition breaks down if the tensor quantity $\tensor{G}$ has determinant less than zero. Therefore, we make sure to choose a manufactured solution for $\tensor{G}$ which abides by this requirement (more on the Method of Manufactured Solutions in the following chapter). In practice, we also square and square-root this quantity to ensure that the numerical solver never dips below zero:

%%
\begin{equation}
J_G \implies \sqrt{\bigl(J_G\bigr)^2}
\end{equation}
%%

The redefinition in \eqref{eq:BreDef} requires a transformation of the time derivative, $\frac{\partial\tensor{B}}{\partial t}$, in Eq.~\eqref{eq:WFEvolNatConfig}:

%%
\begin{align}
	\label{eq:BtTrans}
	\partial_{t}\tensor{B}&=\partial_{t}~({J_{G}^{-1/3}\tensor{G}})
	\\
	&=J_{G}^{-1/3}~\partial_{t}\tensor{G}-\frac{1}{3}J_{G}^{-4/3}~\partial_{t}J_{G}~\tensor{G}.
\end{align}
%%
where $\partial_{t}\tensor{B}$ is shorthand for $\frac{\partial\tensor{B}}{\partial t}$. Using Jacobi's Formula and the definition of the adjugate matrix on $\partial_{t}J_{G}$, we achieve the final form of $\frac{\partial\tensor{B}}{\partial t}$ with the definition of $\tensor{B}$ given in \eqref{eq:BreDef}:
%%
\begin{align}
	\label{eq:Bt}
	\partial_{t}\tensor{B}&=J_{G}^{-1/3}~\partial_{t}\tensor{G}-\frac{1}{3}J_{G}^{-4/3}~(J_{G}~\trace(\tensor{G}^{-1}\partial_{t}\tensor{G}))~\tensor{G}
	\\
	&=J_{G}^{-1/3}\bigl(\partial_{t}\tensor{G}-\frac{1}{3}~\trace(\tensor{G}^{-1}\partial_{t}\tensor{G})~\tensor{G}\bigr).
\end{align}
%%

For ease of calculation, we will apply the Divergence Theorem on the second integral in Eq.~\eqref{eq:WFBLM} and on the second term in Eq.~\eqref{eq:WFEvolNatConfig}, which can be rewritten as: 

%%
\begin{equation}
	\label{eq:Bouterv}
	(\nabla\tensor{B})\bv{v}=\nabla\cdot(\tensor{B}\otimes\bv{v}).
\end{equation}
%%

This gives us our finalized Eulerian weak form, $(W)$:

%%
\begin{align}
	\label{eq:WF1}
	\int_{\Omega_{x}}\trace(\nabla\bv{v})\tilde{p} \, \d{v}&=0,
	\\
	\label{eq:WF2}
	\int_{\Omega_{x}}\rho\biggl[\frac{\partial\bv{v}}{\partial t}+(\nabla\bv{v})\bv{v}\biggr]\cdot\tilde{\bv{v}}\,\d{v}+\int_{\Omega_{x}}\tensor{T}\colon\nabla\tilde{\bv{v}}\, \d{v}-\int_{\partial\Omega_{x}}\tensor{T}\hat{\bv{n}}\cdot\tilde{\bv{v}}\, \d{a}&=0,
	\\
	\int_{\Omega_{x}}\biggl[\frac{\partial\tensor{B}}{\partial t}-(\nabla\bv{v})\tensor{B}-\tensor{B}\transpose{(\nabla\bv{v})}+\frac{1}{\tau}(\tensor{B}-\tensor{I})\biggr]\colon\tilde{\tensor{B}}\, \d{v}-\int_{\Omega_{x}}(\tensor{B}\otimes\bv{v})\colon\nabla{\tilde{\tensor{B}}}\, \d{v} \nonumber
	\\
	\label{eq:WF3}
	+\int_{\partial\Omega_{x}}(\tensor{B}\otimes\bv{v})\hat{\bv{n}}\colon\tilde{\tensor{B}}\, \d{a}&=0,
\end{align}
%%
with
%%
\begin{align}
	\label{eq:Cauchy}
	\tensor{T}&=-p\tensor{I}+\eta(\nabla\bv{v}+\transpose{(\nabla\bv{v})})+\mu(\tensor{B}-\tensor{I})
	\\
	\label{eq:BDef}
	\tensor{B}&=J_{G}^{-1/3}\tensor{G}
	\\
	\label{eq:BtDef}
	\frac{\partial\tensor{B}}{\partial t}&=J_{G}^{-1/3}\Bigl[\frac{\partial\tensor{G}}{\partial t}-\frac{1}{3}~\trace\bigl(\tensor{G}^{-1}\frac{\partial\tensor{G}}{\partial t}\bigr)~\tensor{G}\Bigr].
\end{align}
%%

In this formulation, $\partial\Omega_{x}$ denotes the boundary of our domain, and $\hat{\bv{n}}$ denotes the outward-pointing unit normal vector across that boundary.

\section{Frames of Reference}

In mechanics, there are two main formulations of a system, Lagrangian and Eulerian, each with a distinct frame of reference. A third formulation -- the Arbitrary-Lagrangian-Eulerian (ALE) description of PDEs -- is less common, but is a simple and clever solution when neither the Lagrangian nor the Eulerian description is convenient, e.g. for fluid-structure-interaction or turbulent flow problems. 

An Eulerian formulation is defined according to a fixed, or spatial, coordinate system in real time. The window of observation is within a fixed boundary or a control volume in the current configuration. This description lends itself well to fluid mechanics where we are typically interested in the fluid velocity at a point in space, for example, rather than the velocity of an individual molecule. The Lagrangian formulation, on the other hand, is defined according to a reference, or material, coordinate system, typically corresponding to the initial configuration of the body. Points in the coordinate system correspond to distinct material points. In solid mechanics, it is rather easy to track the motion of individual particles from the reference configuration, therefore the Lagrangian description is more readily used.

However, the Eulerian description breaks down if the boundary of our desired domain is not stationary. Similarly, if the deformation of our material body is very large and nonuniform, the Lagrangian formulation may fail. When we must model both fluid and solid mechanics simultaneously, which formulation do we choose? The ALE formulation allows us to solve problems under one common coordinate system via the selection of a convenient computational domain that corresponds with neither the reference nor the current configurations. To demonstrate the consequences of each choice, we will reformulate and solve the above Eulerian Oldroyd-B problem using Lagrangian and ALE frameworks, comparing and contrasting all three solutions in the next chapter.

\subsection{Lagrangian Formulation}

To obtain the Lagrangian form of the Eulerian weak statement above, we perform a change of variables on Eqns.~\eqref{eq:WF1}--\eqref{eq:WF3} from the current coordinates, $\bv{x}$, to the reference ones, $\bv{X}$. We begin by defining the transformation map from the reference to the current body, adopting the standard notation used in \cite{Hron2014} and \cite{ALEHughes}. 

Let $\Omega_{x}$ be the current domain, as in $(W)$, and $\Omega_{X}$ be the reference domain. We introduce a transformation map, $\varphi \colon \Omega_{X} \to \Omega_{x}$:

%%
\begin{equation}
	\varphi(\bv{X},t)=\bv{x}:=\bv{X}+\bv{u}
\end{equation}
%%

where $\bv{u}$ is the displacement of point $\bv{X}$. The velocity, $\bv{v}$, is simply

%%
\begin{equation}
	\bv{v}=\frac{\partial\varphi(\bv{X},t)}{\partial t}\bigg\rvert_{\bv{X}}=\frac{\partial\bv{u}}{\partial t}.
\end{equation}
%%

Note that in this formulation, the displacement $\bv{u}$ replaces the velocity as our primary unknown. This field, while explicitly present in our equations, will also be hidden within $\tensor{F}$, and $J$. 

For use later, we will define the deformation gradient, $\tensor{F}$, and its determinant, J:

%%
\begin{equation}
	\tensor{F}=\frac{\partial\varphi(\bv{X},t)}{\partial \bv{X}}=\tensor{I}+\frac{\partial\bv{u}}{\partial\bv{X}}=\tensor{I}+\nabla_{X}\bv{u} \ \ ; \ \ J=\det[\tensor{F}].
\end{equation}
%%

The balance of mass, in its Lagrangian form, simplifies to a condition that the determinant of the deformation gradient is equal to 1. This is due to the assumed incompressibility of our material.

%%
\begin{equation}
	\label{eq:BOM}
	\frac{\partial\rho}{\partial t}+ \nabla\cdot(\rho\bv{v})=0 \implies J=1.
\end{equation}
%%

We can now transform the spatial and total time derivatives in Eqns.~\eqref{eq:WF1}-\eqref{eq:WF3}. We begin by performing a change of variables on the gradients:

%%
\begin{equation}
	\nabla_{x}\bv{v}=\frac{\partial \bv{v}}{\partial \bv{x}}=\frac{\partial\bv{v}}{\partial\bv{X}}\frac{\partial\bv{X}}{\partial\bv{x}}=\nabla_{X}\bv{v}~\tensor{F}^{-1}.
\end{equation}
%%

Next, we express the total time derivative of quantity $A$, such as of $\bv{v}$ in Eq.~\eqref{eq:WF2} or of $\tensor{B}$ in Eq.~\eqref{eq:WF3}.

%%
\begin{equation}
	\dot{A}=\frac{\partial A}{\partial t}\bigg\rvert_{X}=\frac{\partial A}{\partial t}\bigg\rvert_{x}+\frac{\partial A}{\partial\bv{x}}\frac{\partial\bv{x}}{\partial t}\bigg\rvert_{X}=\frac{\partial A}{\partial t}\bigg\rvert_{x}+\nabla_{x}A\cdot\bv{v}.
\end{equation}
%%

We handle the last term in Eq.~\eqref{eq:WF2} by applying a corollary of the Piola identity \cite{Oden2008}, which states:

%%
\begin{equation}
	\hat{\bv{n}}\d{a}=J \inversetranspose{\tensor{F}}\hat{\bv{n}}_{X}\d{A}.
\end{equation}
%%

where $\hat{\bv{n}}$ is the outward-pointing normal across the current boundary, and $\hat{\bv{n}}_{X}$ is that across the reference boundary. Lastly, the volume integrals in Eqs.~\eqref{eq:WF1}-\eqref{eq:WF3} are converted to integrals in $\Omega_{X}$ by recognizing that a volume element in $\Omega_{x}$ is a volume element in $\Omega_{X}$ scaled by $J$.

%%
\begin{equation}
	\label{eq:VolumeElement}
	\d{v}=J\d{V}.
\end{equation}
%%

The completed Lagrangian formulation of $(W)$, with the balance of mass replaced with Eq.~\eqref{eq:BOM} and after the application of the Divergence Theorem, is:

%%
\begin{align}
	\label{eq:LWF1}
	\int_{\Omega_{X}}(J-1)\tilde{p}~J\d{V}&=0,
	\\
	\label{eq:LWF2}
	\int_{\Omega_{X}}\rho\frac{\partial^2\bv{u}}{\partial t^2}\cdot\tilde{\bv{u}}\, J\d{V}+\int_{\Omega_{X}} \tensor{T}\inversetranspose{\tensor{F}} \colon\nabla_{X}\tilde{\bv{u}}\, J\d{V}-\int_{\partial\Omega_{X}}J\tensor{T}\inversetranspose{F}\hat{\bv{n}}_{X}\cdot\tilde{\bv{u}}\d{A}&=0,
	\\
	\label{eq:LWF3}
	\int_{\Omega_{X}}\biggl[\frac{\partial\tensor{B}}{\partial t}-(\nabla_{X}\bv{v})\tensor{F}^{-1}\tensor{B}-\tensor{B}\inversetranspose{\tensor{F}}\transpose{(\nabla_{X}\bv{v})}+\frac{1}{\tau}(\tensor{B}-\tensor{I})\biggr]\colon\tilde{\tensor{B}}~ J\d{V}&=0,
\end{align}
%%
with
%%
\begin{equation}
	\label{eq:LCauchy}
	\tensor{T}=-p\tensor{I}+\eta\Bigl[\bigl(\frac{\partial^2\bv{u}}{\partial t\partial{\bv{X}}}\bigr)~\tensor{F}^{-1}+\inversetranspose{\tensor{F}}\transpose{\bigl(\frac{\partial^2\bv{u}}{\partial t\partial{\bv{X}}}\bigr)}\Bigr]+\mu(\tensor{B}-\tensor{I}),
\end{equation}
%%
and $\tensor{B}$ and $\dfrac{\partial\tensor{B}}{\partial t}$ given by Eqs.~\eqref{eq:BDef} and \eqref{eq:BtDef} respectively. Recall that our unknown fields are the scalar quantity $p$, the tensor quantity $\tensor{G}$, and the vector displacement $\bv{u}$, hidden within $\tensor{F}$ and $J$.

\subsection{ALE Formulation}

As mentioned above, the ALE formulation is useful for problems where neither the Eulerian nor the Lagrangian framework is convenient. The mesh is built on a computational domain that is not tied to the body motion or to the reference configuration. The "mesh motion" is selected to have a velocity equal to the material velocity on the boundary, but be the solution to a well-behaved differential equation on the interior; in this case, we use the Laplace equation. This solves the Lagrangian issue of mesh-entanglement when the fluid experiences turbulence within the domain, and the Eulerian limitation of stationary boundaries \cite{Hron2014}. Note that this mesh motion introduces yet another unknown and weak contribution to the system for a total of four unknown variables.

To formulate the ALE problem, we will again perform a change of variables on Eqs.~\eqref{eq:WF1}-\eqref{eq:WF3} from coordinates in the current configuration, $\bv{x}$, to coordinates in our computational domain, $\bv{\chi}$. Let our transformation $\hat{\varphi} \colon \Omega_{\chi} \to \Omega_{x}$ be defined by:

%%
\begin{equation}
	\hat{\varphi}(\bv{\chi},t)=\bv{x}:=\bv{\chi}+\hat{\bv{u}}
\end{equation}
%%

Here, $\hat{\bv{u}}$ is our "mesh motion", and it represents the displacement from point $\bv{\chi}$ to $\bv{x}$. As previously stated, this field is the solution to the Laplace equation on the interior of $\Omega_{\chi}$, and its time derivative is equal to the material velocity on the boundary $\partial\Omega_{\chi}$:

\[
\hat{\bv{u}} = \begin{cases} 
	-\nabla\cdot\nabla\hat{\bv{u}}=0 & \text{in }\Omega_{\chi} \\
	\dfrac{\partial\hat{\bv{u}}}{\partial t}=\bv{v} & \text{on } \partial\Omega_{\chi} \\
\end{cases}
\]

Following the same procedure as in the Lagrangian case, we will introduce the deformation gradient from $\Omega_{\chi}$ to $\Omega_{x}$ and its determinant, $\hat{\tensor{F}}$ and $\hat{J}$ respectively.

%%
\begin{equation}
	\hat{\tensor{F}}=\frac{\partial\hat{\varphi}(\bv{\chi},t)}{\partial \bv{\chi}}=\tensor{I}+\frac{\partial\hat{\bv{u}}}{\partial\bv{\chi}}=\tensor{I}+\nabla_{\chi}\hat{\bv{u}} \ \ ; \ \ \hat{J}=\det[\hat{\tensor{F}}].
\end{equation}
%%

Next, we change the variables of the velocity gradients:

%%
\begin{equation}
	\nabla_{x}\bv{v}=\frac{\partial \bv{v}}{\partial \bv{x}}=\frac{\partial\bv{v}}{\partial\bv{\chi}}\frac{\partial\bv{\chi}}{\partial\bv{x}}=\nabla_{\chi}\bv{v}~\hat{\tensor{F}}^{-1}.
\end{equation}
%%

To express the total time derivative of a quantity $A$, we will first write the local time derivative.

%%
\begin{equation}
	\frac{\partial A}{\partial t}\bigg\rvert_{\chi}=\frac{\partial A}{\partial t}\bigg\rvert_{x}+\frac{\partial A}{\partial\bv{x}}\frac{\partial\bv{x}}{\partial t}\bigg\rvert_{\chi}=\frac{\partial A}{\partial t}\bigg\rvert_{x}+\nabla_{x}A\cdot\frac{\partial\hat{\bv{u}}}{\partial t}.
\end{equation}
%%

Subtracting the last term on the right hand side and adding $\nabla_{x}A\cdot\bv{v}$ to both sides, we obtain the total time derivative of $A$, such as of $\bv{v}$ in Eq.~\eqref{eq:WF2} or of $\tensor{B}$ in Eq.~\eqref{eq:WF3}.

%%
\begin{align}
	\dot{A}&=\frac{\partial A}{\partial t}\bigg\rvert_{x}+\nabla_{x}A\cdot\bv{v} \nonumber
	\\
	&=\frac{\partial A}{\partial t}\bigg\rvert_{\chi}+\nabla_{x}A\cdot(\bv{v}-\frac{\partial\hat{\bv{u}}}{\partial t}) \nonumber
	\\
	&=\frac{\partial A}{\partial t}\bigg\rvert_{\chi}+(\nabla_{\chi} A)\cdot\hat{\tensor{F}}^{-1}(\bv{v}-\frac{\partial\hat{\bv{u}}}{\partial t})
\end{align}
%%
%Is there a mistake in \tensor{F}^{-1}?? On the wrong side of the inner product

The final ALE formulation, after the application of the Divergence Theorem and including the weak contribution of the mesh motion, is:

%%
\begin{align}
	\label{eq:ALEWF1}
	\int_{\Omega_{\chi}}\trace(\nabla_{\chi}\bv{v}\hat{\tensor{F}}^{-1})\tilde{p}~\hat{J}\d{V}&=0,
	\\
	\label{eq:ALEWF2}
	\int_{\Omega_{\chi}}\rho\biggl[\frac{\partial \bv{v}}{\partial t}+(\nabla_{\chi}\bv{v})\cdot\hat{\tensor{F}}^{-1}(\bv{v}-\frac{\partial\hat{\bv{u}}}{\partial t})\biggr]\cdot\tilde{\bv{v}}\, \hat{J}\d{V}+\int_{\Omega_{\chi}} \hat{J}\tensor{\hat{T}}\inversetranspose{\hat{\tensor{F}}} \colon\nabla_{\chi}\tilde{\bv{v}}\, \d{V} \nonumber
	\\
	-\int_{\partial\Omega_{\chi}}\hat{J}\hat{\tensor{T}}\inversetranspose{\hat{\tensor{F}}}\hat{\bv{n}}_{\chi}\cdot\tilde{\bv{v}}\d{A}&=0,
	\\
	\int_{\Omega_{\chi}}\biggl[\frac{\partial\tensor{B}}{\partial t}-(\nabla_{\chi}\bv{v})\hat{\tensor{F}}^{-1}\tensor{B}-\tensor{B}\inversetranspose{\hat{\tensor{F}}}\transpose{(\nabla_{\chi}\bv{v})}+\frac{1}{\tau}(\tensor{B}-\tensor{I})\biggr]\colon\tilde{\tensor{B}}~ \hat{J}\d{V} \nonumber
	\\
	-\int_{\Omega_{\chi}}\Bigl(\tensor{B}\otimes\bigl(\hat{\tensor{F}}^{-1}(\bv{v}-\frac{\partial\hat{\bv{u}}}{\partial t})\bigr)\Bigr)\colon\nabla{\tilde{\tensor{B}}}\, \hat{J}\d{V} \nonumber
	\\
	\label{eq:ALEWF3}
	+\int_{\partial\Omega_{\chi}}\Bigl(\tensor{B}\otimes\bigl(\hat{\tensor{F}}^{-1}(\bv{v}-\frac{\partial\hat{\bv{u}}}{\partial t})\bigr)\Bigr)\, \hat{\bv{n}}\colon\tilde{\tensor{B}}\, \hat{J} \d{A}&=0,
%%should there be a J at the end of the last integral??
	\\
	\label{eq:ALEWF4}
	\int_{\Omega_{\chi}}\nabla_{\chi}\hat{\bv{u}}\colon\nabla_{\chi}\tilde{\bv{u}}\, \d{V}+\int_{\partial\Omega_{\chi}}\biggl(\frac{1}{\gamma\text{h}}(\bv{v}-\frac{\partial\hat{\bv{u}}}{\partial t})-(\nabla_{\chi}\hat{\bv{u}})\hat{\bv{n}}\biggr)\cdot\tilde{\bv{u}}\, \d{A} &= 0
\end{align}
%%

where 

%%
\begin{equation}
	\label{eq:CauchyALE}
	\hat{\tensor{T}}=-p\tensor{I}+\eta\bigl((\nabla_{\chi}\bv{v})~\hat{\tensor{F}}^{-1}+\inversetranspose{\hat{\tensor{F}}}\transpose{(\nabla_{\chi}\bv{v})}\bigr)+\mu(\tensor{B}-\tensor{I}).
\end{equation}
%%

$\tensor{B}$ and $\dfrac{\partial\tensor{B}}{\partial t}$ are given by Eqs.~\eqref{eq:BDef} and \eqref{eq:BtDef} respectively. $\tilde{\bv{u}}$ is now the test function for $\hat{\bv{u}}$, $\gamma$ is a small positive parameter, and h is the characteristic mesh length. The coefficient $\dfrac{1}{\gamma\text{h}}$ was chosen to resemble those in Nitsche's Method for applying general boundary conditions \cite{Nitsche2009}, and it weighs the applied boundary condition more heavily than the term arising from the application of the Divergence Theorem. Our four unknown quantities are the mesh motion $\hat{\bv{u}}$ (hidden inside $\hat{\tensor{F}}$ and $\hat{J}$), the fluid velocity $\bv{v}$, the pressure $p$, and the tensor $\tensor{G}$.








