%!TEX root = ../YourName-Dissertation.tex
\chapter{Conclusion}

\section{Future Plans}

The results of the clot problem, presented in the preceding chapter, require further exploration of time-integration schemes to ensure the steady convergence of our quantities of interest. Pending the resolution of these difficulties, we will begin to apply the solver to produce meaningful results for acute stroke therapy, specifically geared toward exploring the clot-removal procedure outlined by Dr.~Simon in \cite{Simon2002}. Such a goal will require further enhancements of our model.

\subsection{3D Verification Analysis}

In this study, we have only focused on a two-dimensional formulation of the problem. Clearly, the next step is to expand our system to three-dimensions to capture the full behavior of a lodged blood clot. We believe that this step requires the use of an iterative, rather than a direct, solver with the appropriate preconditioner applied to our coefficient matrix. 

Recall that the finite element method reduces a fully nonlinear system of differential equations to a simple matrix statement of the form $\tensor{A}\bv{x}=\bv{b}$ at each time step, where $\bv{x}$ represents our vector of unknowns \cite{Hughes2000-1}. Therefore, the challenge in a finite element scheme lies in the numerical inversion of the matrix $\tensor{A}$ to solve for $\bv{x}$. For both problems presented in this document, a direct solver was sufficient to invert our coefficient matrix via a derivative of LU-decomposition. However, in three-dimensions, the size of this matrix increases drastically, and its unique properties play a larger role in its invertibility. For example, the underlying coefficient matrix of our system has a block structure with a zero block on its diagonal, stemming from the incompressibility condition and the resulting coupling between the pressure quantity $p$ and its corresponding test function $\tilde{p}$. This results in singular submatrices which are unable to be inverted directly. This attribute is common amongst incompressible Navier-Stokes-type systems, such as ours, where $p$ acts as a Lagrange multiplier, enforcing the $\ldiv(\bv{v})=0$ constraint on the velocity.

Luckily, mathematicians in the field have studied and published works on the selection of appropriate preconditioners to such problems based on the block structure of $\tensor{A}$ and the Schur complement of a submatrix (\cite{Cao2014},\cite{Notay2014}, \cite{Elman2002}), some of which include applications to the Stokes or Navier-Stokes systems. The objective of a preconditioner is to "reduce the number of iterations required for convergence while not increasing significantly the amount of computation required
at each iteration" \cite{Murphy2000}. We will proceed by reviewing the literature on iterative methods and preconditioners for saddle point problems and adopting the established techniques.

\subsection{Physiological Auxiliary Conditions}

The next step is to apply the 3D model to a clinical situation, specifically that of the aforementioned mechanical thrombectomy procedure. The solution would be carried out over a cylindrical clot domain, to resemble a clot fully enveloped by a cylindrical artery, with physiological boundary and initial conditions. Specifically, we solve the model under no-slip boundary conditions with an applied traction force on one face of the cylinder to represent the alternating suction pressure as a first approximation of the proposed procedure. The true stress would then be studied at the clot boundary, with expectations of clot detachment at the arterial wall. We recognize that cerebral blood clots are rarely perfectly cylindrical, but this is one step forward from the cubic verification domain.

\subsection{Chemical Contributions to Fluid Model}

The fluid model simulated in this project to describe blood, though a good first approximation to the viscoelastic tendencies of blood, has its limitations. As discussed in Chapter~5, it treats blood as a homogeneous continuum with a constant viscosity, disregarding the heterogenous nature of blood. The viscosity of blood, at lower shear rates, can not be precisely approximated by a constant value, and depends on the shear rate of flow. In this study, we have used parameter values that are associated with normal 40\% hematocrit, but pathology and the coagulation process vary these component concentrations and, subsequently, the properties of the fluid. In practice, it is difficult to capture the fully-coupled biochemical, rheological, and mechanical effects on clot behavior. The blood model developed by Rajagopal et.~al. was a big step toward this goal:
%%
\begin{align}
	\tensor{T}&=-p\tensor{I}+\tensor{S},
	\\
	\tensor{S}&=\mu\tensor{B}_{\kappa_{p(t)}}+\eta_{1}\tensor{D},
	\\
	\UCTD{\tensor{B}}_{\kappa_{p(t)}}&=-2\bigl(\frac{\mu}{\alpha}\bigr)^{1+2n}\bigl(\trace{\tensor{B}_{\kappa_{p(t)}}}-3\lambda \bigr)^n \bigl[\tensor{B}_{\kappa_{p(t)}}-\lambda\tensor{I} \bigr],
	\\
	\lambda&=\frac{3}{\trace{\tensor{B}^{-1}_{\kappa_{p(t)}}}},
	\\
	n&=\frac{\gamma-1}{1-2\gamma};n>0
\end{align}
%%

In \cite{Anand2004Visco}, \cite{Anand2006Mech}, and \cite{Anand2006Form}, the authors track the concentrations of nearly 30 enzymes, proteins, and platelets present in the extrinsic pathway of coagulation to fully predict clot formation and lysis. We will incorporate a subset of these chemical reactions in our next fluid model to test our ability to couple constituent concentrations with the overall flow characteristics, described by the equations above.

\subsection{Surrounding Fluid and Artery}

Thrombus formation does not occur in isolation; there are numerous external influences that factor into the complete solution of our problem. The method proposed by Dr.~Simon is an alternating suction pressure \textit{mediated through blood or saline} \cite{Simon2002}. Without the presence of these fluids in our simulations, we can not depict the realistic effects of the oscillating pressure forces. The cerebral artery plays a significant role in the reaction of the local thrombus site, as well. Blood vessels in the body are known to be elastic, adapting to pulsatile flow, and typically elliptical in shape due to the surrounding musculature \cite{Holzapfel2010}. Approximating the clot response within a rigid cylindrical tube does not provide the most accurate results. Along with this elasticity, there is a possibility of arterial collapse at high magnitudes of pressure \cite{Romero2012}, proving further relevance of the vessel wall in our simulations.

In the coming years, we would like to synthesize models of both a functioning artery and thrombus to depict their interactions more realistically. Blood or saline as the fluid surrounding the clot would introduce a third substance to our mesh for a total of three sets of material constants. This venture would require the use of transformer (TF) elements to translate between material elements, as outlined in \cite{Romero2012}.

\section{Conclusion}

The impetus of this thesis is a proposed mechanical thrombectomy procedure for the therapy of acute ischemic stroke patients. Such a method must be numerically modeled before it can be accepted into practice. Therefore, we have developed a simulation scheme in \COMSOL to model the behavior of various viscoelastic fluids. We tested its ability with an Oldroyd-B fluid, an then with a fluid model that has been accepted to represent human blood (\cite{Anand2004Visco}, \cite{Anand2006Mech}, \cite{Anand2006Form}). The constitutive relationships models were developed using a thermodynamical framework \cite{Rajagopal2000}, governed by specific energy dissipation and storage functions. The governing system of equations, comprised of the balance of mass, momentum, and a strain-evolution law, is converted from its Eulerian form to its Lagrangian and ALE descriptions. With our choices regarding the finite element and time-integration schemes, we applied the Method of Manufactured Solutions to each formulation to assess the accuracy of the solver in two-dimensions. Overall, the solver delivers the pressure, velocity and strain solutions with scant error when solving the system for an Oldroyd-B fluid. For the blood case, the solutions decrease in error with mesh refinement for all three formulations, but not at the expected rates; some faster than expected and some slower. The solver requires further study and development of a proper time-integration scheme before moving forward with simulating the thrombectomy procedure in \cite{Simon2002}.
