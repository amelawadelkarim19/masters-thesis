%!TEX root = ../YourName-Dissertation.tex
\chapter{\COMSOL Inputs}

Thus far, we have decided upon the set of differential equations that govern our example problem. We then transformed the Eulerian strong form of the problem, $(S)$, into its weak form, $(W)$ and reformulated the problem as both Lagrangian and Arbitrary-Lagrangian-Eulerian (ALE) using a mesh motion that is the solution to the Laplace equation. Now, we begin the process of numerically solving these integral equations for our unknowns using \COMSOL. In this Chapter, we give the reader a brief introduction to the finite element method, describe the solver setup, and establish our verification procedure.

\section{Introduction to the Finite Element Method}
\COMSOL is a user-friendly simulation software that offers modules for the solution of a variety of physical problems along with a platform for the development of new finite element methods.\footnote{For a exhaustive presentation of the FEM see \cite{Hughes2000-1}.} In Chapter~2, we have identified the strong form of our problem and its corresponding weak forms. Both the strong form and the weak form are equivalent statements of the original problem --- a proof of which can be found in \cite{Hughes2000-1}.

To formulate the weak form of a set of governing equations, we first need to define two spaces of functions: one is called the solution space, $\mathcal{S}$, and the other is called the space of test functions, or $\mathcal{V}$. Both spaces carry functions that are $H^1$, meaning that said functions are square-integrable along with their first order derivatives. However, those in $\mathcal{S}$ are designed to satisfy the Dirichlet boundary conditions imposed on the solution while those in $\mathcal{V}$, called test functions, take on the homogenous-counterpart to those boundary conditions, that is, they equal zero on the Dirichlet boundary. 

To write this mathematically, we partition the boundary of our domain into two disjoint sets: one where Dirichlet data are prescribed ($\Gamma_g$), and one where Neumann data are prescribed ($\Gamma_h$). The closure of the union of these two sets equals the entire boundary. If the Dirichlet data is denoted as $g$, then we can define these spaces as
%%
\begin{align}
	\label{eq:SolSpace}
	\mathcal{S}&=\{u~|~u \in H^1,u\,(\Gamma_g)=g\},
	\\
	\label{eq:VarSpace}
	\mathcal{V}&=\{w~|~w \in H^1,w\,(\Gamma_g)=0\}.
\end{align}
%%
The spaces $\mathcal{S}$ and $\mathcal{V}$ are infinite-dimensional; the number of integral equations necessary to solve a weak problem exactly in an infinite-dimensional context would be infinite. In order for the problem to be solved numerically, we must approximate the underlying infinite-dimensional function spaces with corresponding finite-dimensional spaces. The core concept underlying any FEM is to achieve the approximation of the function spaces by splitting our domain up into a finite number of subdomains, often referred to as elements. This partition is called a \emph{triagulation} or a mesh. The size of these approximate spaces is dependent on the mesh chosen, parameterized by a characteristic mesh length, h; the finer the mesh, the larger the set of the test functions becomes. We label these finite subsets of the solution and test-function spaces $\mathcal{S}^h$ and $\mathcal{V}^h$ respectively. 

Past this point, the weak formulation is converted into an approximate set of integral equations, and ultimately into an algebraic matrix equation. The transformation is outlined in \cite{Hughes2000-1}. The objective of the finite element method is to find the function $u^h \in \mathcal{S}^h$ that, for every test function $v^h \in \mathcal{V}^h$, satisfies the governing system of equations.

The test functions, $v^h$, are defined as the linear combination of a set of \emph{shape functions}. These shape functions are typically Lagrange polynomials supported over patches of contiguous elements, meaning they are zero everywhere except over a specific patch. The shape function can be of varying degrees. Our choices for these polynomials will be discussed in a later section.


\section{Method of Manufactured Solutions}

The computer implementation of a numerical method often suffers from programming errors.  Hence, certain measures must be taken to ensure that the the simulation software is solving the equations at hand correctly. This process is called \emph{verification} and is not to be mistaken for a follow up process in computational software for physics based problems called \emph{validation}.  This latter development consists in comparing the numerical predictions of a calculation with benchmark experiments to ascertain the limits within which the numerical solution is an acceptable predictor of an actual physical phenomenon. 

The instinctual approach to code verification would be to compare the numerical results with an existing closed-form solution.  However, for complex nonlinear problems, closed form solutions are seldom available.  Hence, the necessity to develop a standard strategy for verification that does not rely on existing closed-form solution.  The MMS is such a strategy \cite{Salari2000}. The method works as follows:
\begin{enumerate}
\item
Select arbitrary functions for each of the unknown fields and declare such functions to be the exact solution to the problem.

\item
Substitute said functions into the strong form of the governing equations and solve these equations for the source terms that are consistent with the selected exact solution.

\item
Repeat the step above but this time to solve for the boundary and initial conditions that are consistent with the chosen exact solution.

\item
Finally, input the derived source terms and boundary conditions into the code and check whether or not the corresponding numerical solution matches the set of functions selected at the outset of the procedure.
\end{enumerate}

In order to check how well the solutions match, we use error norms to quantitatively describe the error of a solution. Denoting the exact and approximate solutions to a system $u$ and $\hat{u}$, respectively, the error in the approximate solution is said to be $e=|u-\hat{u}|$. If \bv{e} and \tensor{E} are first and second order tensors of error, the $L^{2}$ and $H^1$ norms of \bv{e} are given in Eq.~\eqref{eq:VectorNorms} respectively, and for \tensor{E} in Eq.~\eqref{eq:TensorNorms}:

%%
\begin{align}
	\label{eq:VectorNorms}
	\|\bv{e}\|_{L^{2}}&=\sqrt{\int_{\Omega} \bv{e}\cdot\bv{e} \, \d{v}} & \|\bv{e}\|_{H^{1}}&=\sqrt{\int_{\Omega} \nabla\bv{e} \colondot \nabla\bv{e} \, \d{v}},
	\\
	\label{eq:TensorNorms}
	\|\tensor{E}\|_{L^{2}}&=\sqrt{\int_{\Omega} \tensor{E}\colondot\tensor{E} \, \d{v}} & \|\tensor{E}\|_{H^{1}}&=\sqrt{\int_{\Omega} \nabla\tensor{E}\colondot\nabla\tensor{E} \, \d{v}},
\end{align}
%%

where $\Omega$ is the domain. We will calculate the $L^{2}$ norm of the error for each of our unknown quantities, and the $H^1$ norm of the error for the velocity, displacement, and mesh-motion fields. We do not calculate the $H^{1}$ norm of the error for $p$ or $\tensor{G}$ because the governing equations for these fields do not possess terms that resemble a a bound for this type of norm. Therefore, we do not expect convergence in the spatial derivatives of these quantities.

\subsection{Manufactured Solutions}

Each of the manufactured solutions were chosen with seven guidelines in mind that ensure the ease with which our numerical solutions can be computed. These guidelines can be found in the report on the MMS done by Sandia National Laboratories \cite{Salari2000}. The solutions are given over a square domain, and are the same for all three formulations -- Eulerian, Lagrangian, and ALE -- to ease the comparison later on. 

\subsubsection{Pressure}

The solution for $p$, in all three formulations, is chosen to be a simple oscillating sinusoidal function, given in 2D by,

%%
\begin{equation}
	\label{eq:MSp}
	\MS{MSp}(x,y,t)=p_{0} \cos\bigl(2\pi\,\frac{t}{t_{0}}\bigr)\cos\bigl(2\pi\,\frac{x+y}{L}\bigr),
\end{equation}
%%

with $p_0=\np[Pa]{1}$, $L=\np[m]{1}$, and $t_0=\np[s]{1}$. This function is depicted in Fig.~\ref{fig:MSp}.

%
\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{Chapter-3/Figures/MSp_7}
\caption{Plot of $\MS{MSp}$ at time $t=\np[s]{0.7}$.}
\label{fig:MSp}
\end{figure}
%

\subsubsection{Velocities and Displacements}

For the manufactured fields $\bv{v}$ in the Eulerian, $\hat{\bv{u}}$ in the ALE, and $\bv{u}$ in the Lagrangian formulation, we have selected the same solution:

%%
\begin{align}
	\label{eq:MSv}
	\bv{\MS{MSv}}(x,y,t)&=v_{0}\sin\bigl(2\pi\,\frac{t}{t_{0}}\bigr)\biggl[\sin\bigl(2\pi\,\frac{x+y}{L}\bigr) \, \ui + \cos\bigl(2\pi\,\frac{x-y}{L}\bigr) \, \uj \biggr],
	\\
	\label{eq:MSu}
	\bv{\MS{MSu}}(x,y,t)&=u_{0}\sin\bigl(2\pi\,\frac{t}{t_{0}}\bigr)\biggl[\sin\bigl(2\pi\,\frac{x+y}{L}\bigr) \, \ui + \cos\bigl(2\pi\,\frac{x-y}{L}\bigr) \, \uj \biggr],
	\\
	\label{eq:MSuhat}
	\bv{{\MS{MS\hat{u}}}}(x,y,t)&=\hat{u}_{0}\sin\bigl(2\pi\,\frac{t}{t_{0}}\bigr)\biggl[\sin\bigl(2\pi\,\frac{x+y}{L}\bigr) \, \ui + \cos\bigl(2\pi\,\frac{x-y}{L}\bigr) \, \uj \biggr],
\end{align}
%%

where $v_{0}=\np[m/s]{0.01}$, $u_{0}=\hat{u}_{0}=\np[m]{0.01}$, and $\ui$, $\uj$, $\uk$ are the base vectors of the underlying Cartesian coordinate system.  The plot of the $\ui$-component of these functions at time $t=\np[s]{0.7}$ is given in Fig.~\ref{fig:MSv1}.

%
\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{Chapter-3/Figures/MSv1_7} 
\caption{Plot of the $\ui$-component of $\bv{\MS{MSv}}$, $\bv{\MS{MSu}}$ and ${\bv{\MS{MS\hat{u}}}}$ at time $t=\np[s]{0.7}$.}
\label{fig:MSv1}
\end{figure}
%

The velocity solution in the ALE formulation was simply chosen to be equal to the time derivative of $\bv{{\MS{MS\hat{u}}}}$ to automatically satisfy the imposed boundary constraint:

%%
\begin{equation}
	\label{eq:MSv_ALE}
	\bv{\MS{MSv}}_{A}(x,y,t)=\frac{2\pi \hat{u}_{0}}{t_{0}}\cos\bigl(2\pi\,\frac{t}{t_{0}}\bigr)\biggl[\sin\bigl(2\pi\,\frac{x+y}{L}\bigr) \, \ui + \cos\bigl(2\pi\,\frac{x-y}{L}\bigr) \, \uj \biggr].
\end{equation}
%%

Note that this choice is arbitrary and does not effect the solvability of this system; another choice would simply result in a source term not equal to zero for that constrain equation -- see section on Manufactured Source Terms. The plot of this function at time $t=\np[s]{0.7}$ is given in Fig.~\ref{fig:MSv1ALE}.

%
\begin{figure}[h]
\centering
\includegraphics[scale=0.74]{Chapter-3/Figures/MSv1ALE_7} 
\caption{Plot of the $\ui$-component of the $\bv{\MS{MSv}}_{A}$ at time $t=\np[s]{0.7}$.}
\label{fig:MSv1ALE}
\end{figure}
%

\subsubsection{Strain}

This selected field must have a determinant greater than zero, as the strain tensor $\tensor{B}$ in our formulation is defined as $J_{G}^{-1/3}\tensor{G}$. We have decided upon a function with a determinant equal to 1 everywhere. This field was manufactured by generating an arbitrary 2x2 matrix, $\tensor{\MS{Gg}}$, then diving every component by the square-root of its determinant, $J_{Gg}$, to obtain a 2x2 matrix with determinant equal to 1, \tensor{\MS{GG}}. Finally, this matrix is expanded to a 3x3 to match the \emph{physical} dimension of the problem by setting the $\uk \otimes \uk$ component equal to 1. This must be the case since the \emph{mathematical} problem is two-dimensional. Ultimately, the manufactured field $\tensor{\MS{MSG}}$ has linear and sinusoidal components over the domain at each time step, and its $\ui \otimes \ui$ component can be seen in Fig.~\ref{fig:MSG11}:

%%
\begin{align}
\tensor{\MS{Gg}}=G_0\biggl[\bigl(1+\frac{x}{L}\bigr) \ui \otimes \ui + 0.1\sin\bigl(2\pi\frac{t}{t_0}\bigr)\cos\bigl(2\pi\frac{x+y}{L}\bigr)\ui \otimes \uj \nonumber
\\
+ 0.1\sin\bigl(2\pi\frac{t}{t_0}\bigr)\cos\bigl(2\pi\frac{x+y}{L}\bigr)\uj \otimes \ui +  \bigl(1+\frac{y}{L}\bigr) \uj \otimes \uj \biggr],
\\
\tensor{\MS{GG}}=J_{Gg}^{-1/2}\tensor{\MS{Gg}}
\\
\label{eq:MSG}
\tensor{\MS{MSG}}=\biggl[ GG_{ii} \, \ui \otimes \ui + GG_{ij} \, \ui \otimes \uj + GG_{ji} \, \uj \otimes \ui +  GG_{jj} \, \uj \otimes \uj + 1 \, \uk \otimes \uk \biggr].
\end{align}
%%
%want to align these. and is there a better way of writing this?

Here, $G_0=1$. The $\ui \otimes \ui$ component of $\tensor{\MS{MSG}}$ is depicted in Fig.~\ref{fig:MSG11}.

%
\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{Chapter-3/Figures/MSG11_7} 
\caption{$\ui \otimes \ui$ component of $\tensor{\MS{MSG}}$ at time $t=\np[s]{0.7}$.}
\label{fig:MSG11}
\end{figure}
%

\subsection{\COMSOL Inputs}

Using \Mathematica, we can generate the necessary parameter lists, field variables, source term variables, and weak contributions for the solution of the problem in \COMSOL. These fields are created from the inputs of the physics, the principle unknowns, and the three weak formulations. 

As an example, the notebook converts the Eulerian interior weak contribution of the balance of linear momentum, i.e., the integrands over $\Omega_{x}$ of Eq.~\eqref{eq:WF2}, from

%%
\begin{equation}
	\rho\biggl[\frac{\partial\bv{v}}{\partial t}+(\nabla\bv{v})\bv{v}\biggr]\cdot\tilde{\bv{v}}+\tensor{T}\colon\nabla\tilde{\bv{v}},
\end{equation}
%%
to its scalar expansion in a \COMSOL compatible syntax with all terms properly expanded:
%%

\begin{verbatim}
-(p*(test(v1x)+test(v2y)))+(mu*(G11*test(v1x)+G12*(test(v1y)
+test(v2x))+G22*test(v2y)-(-G12^2+G11*G22)^(1/3)*(test(v1x)
+test(v2y))))/(-G12^2+G11*G22)^(1/3)+rho*test(v1)*v1t
+rho*test(v1)*v1*v1x+rho*test(v1)*v1y*v2+rho*test(v2)*v2t
+rho*test(v2)*v1*v2x+rho*test(v2)*v2*v2y+eta*(2*test(v1x)*v1x
+(test(v1y)+test(v2x))*(v1y+v2x)+2*test(v2y)*v2y)-test(v1)*vEqS1
-test(v2)*vEqS2
\end{verbatim}

%%

One should note that the outputted weak contribution is generated under given assumptions about the quantities involved. We have provided that the tensor $\tensor{G}$ is symmetric, and therefore $G_{21}$ is equal to $G_{12}$. All cases of the former are replaced by the latter. Furthermore, this outputted contribution includes the source term for the balance of momentum, which is not present in Eq.~\eqref{eq:WF2}, but will be discussed in the next section.

\subsubsection{Manufactured Source Terms}

To implement the MMS, as stated before, we must manufacture solutions that follow a set of guidelines \cite{Salari2000}, then work backwards to obtain the source terms and auxiliary conditions that would produce our desired results. The full Eulerian strong form of our problem, with the source terms present, is

%%
\begin{align}
	\label{eq:FullIncompressibility}
	\ldiv_{x}(\bv{v})=\trace(\nabla_{x}\bv{v})&=c_E,
	\\
	\label{eq:FullBLM}
	\rho\biggl[\frac{\partial\bv{v}}{\partial t}+(\nabla_{x}\bv{v})\bv{v}\biggr]-\ldiv_{x}(\tensor{T})&=\bv{f}_E,
	\\
	\label{eq:FullHronEvolNatConfig}
	\frac{\partial\tensor{B}}{\partial t}+\nabla_{x}\cdot(\tensor{B}\otimes\bv{v})-(\nabla_{x}\bv{v})\tensor{B}-\tensor{B}\transpose{(\nabla_{x}\bv{v})}+\frac{1}{\tau}(\tensor{B}-\tensor{I})&=\tensor{A}_E,
\end{align}
%%

supplemented by Eqs.~\eqref{eq:Cauchy}-\eqref{eq:BtDef}. In the Lagrangian form, the strong form, with the corresponding source terms, becomes

%%
\begin{align}
	\label{eq:LFullIncomp}
	J-1&=c_L,
	\\
	\label{eq:LFullBLM}
	\rho\frac{\partial^2\bv{u}}{\partial t^2}-\ldiv_{X}(J\tensor{T}\inversetranspose{\tensor{F}})&=\bv{f}_L,
	\\
	\label{eq:LFullEvol}
	\frac{\partial\tensor{B}}{\partial t}-(\frac{\partial^2\bv{u}}{\partial t \partial\bv{X}})\tensor{F}^{-1}\tensor{B}-\tensor{B}\inversetranspose{\tensor{F}}\transpose{(\frac{\partial^2\bv{u}}{\partial t \partial\bv{X}})}+\frac{1}{\tau}(\tensor{B}-\tensor{I})&=\tensor{A}_L,
\end{align}
%%

along with Eq.~\eqref{eq:LCauchy} and Eqs.~\eqref{eq:BDef}-\eqref{eq:BtDef}. Finally, the full ALE strong form, is 

%%
\begin{align}
	\label{eq:ALEFullIncomp}
	\trace(\nabla_{\chi}\bv{v}\hat{\tensor{F}}^{-1})&=c_A,
	\\
	\label{eq:ALEFullBLM}
	\rho\frac{\partial \bv{v}}{\partial t}+(\nabla_{\chi}\bv{v})\cdot\hat{\tensor{F}}^{-1}(\bv{v}-\frac{\partial\hat{\bv{u}}}{\partial t})-\ldiv_{\chi}(\hat{J}\tensor{\hat{T}}\inversetranspose{\hat{\tensor{F}}})&=\bv{f}_A,
	\\
	\label{eq:ALEFullEvol}
	\frac{\partial\tensor{B}}{\partial t}+\nabla_{\chi} \cdot\Bigl[\tensor{B}\otimes\bigl(\hat{\tensor{F}}^{-1}(\bv{v}-\frac{\partial\hat{\bv{u}}}{\partial t})\bigr)\Bigr]-(\nabla_{\chi}\bv{v})\hat{\tensor{F}}^{-1}\tensor{B}-\tensor{B}\inversetranspose{\hat{\tensor{F}}}\transpose{(\nabla_{\chi}\bv{v})}+\frac{1}{\tau}(\tensor{B}-\tensor{I})&=\tensor{A}_A,
	\\
	\label{eq:ALEFullMM}
	-\nabla_{\chi}\cdot\nabla_{\chi}\hat{\bv{u}}&=\bv{w}_A,
\end{align}

supplemented by Eq.~\eqref{eq:CauchyALE} and Eqs.~\eqref{eq:Cauchy}-\eqref{eq:BtDef}. Substituting the chosen manufactured functions for $p$, $\bv{v}$, $\bv{u}$, $\hat{\bv{u}}$, and $\tensor{G}$ into their respective systems, we calculate the source terms. These calculations are tedious and prone to error, hence we use the symbol manipulation capability of \Mathematica to carry out this task.

\subsubsection{Initial and Boundary Conditions}

To calculate the initial conditions of each dependent quantity, we simply plug $t=0$ into each of our solutions and obtain $\MS{MSp}_i$, $\bv{\MS{MSv}}_i$, $\bv{\MS{MSu}}_i$, $\bv{\MS{MS\hat{u}}}_i$, and $\tensor{MSG}_i$. Next, we gather our desired boundary conditions for the velocity. The Dirichlet data, defined on $\Gamma_g$, is retrieved by evaluating \bv{\MS{MSv}} at the boundary, and the Neumann data, defined on $\Gamma_h$, is calculated via the so-called Cauchy law \cite{GurtinFried_2010_The-Mechanics_0}:
%%
\begin{equation}
	\label{eq:MSNeumannBC}
	\tensor{T}\hat{\bv{n}} = \bv{\MS{MSgh}},
\end{equation}
%%
where $\hat{\bv{n}}$ is the outward unit normal to the boundary and \bv{\MS{MSgh}} is the desired Neumann datum vector field. For the sake of our case study, we will impose pure Dirichlet boundary conditions on $\bv{v}$ (on $\bv{u}$ in the Lagrangian case). That is, the exact solutions for $\bv{v}$ and $\bv{u}$ are prescribed on \emph{entire} boundary in their respective systems. Because the solution for $p$ is not unique with pure Dirichlet boundary conditions for $\bv{v}$ or $\bv{u}$, we apply an additional constraint that says that the average of the quantity $p$ equals 0, which is true of our manufactured solution.

Lastly, along with the material parameters, we have introduced a number of new constants in the creation of our manufactured solutions that must be understood by \COMSOL, namely $t_{0}$, $L$, $p_{0}$, $v_{0}$, $u_{0}$, $\hat{u}_{0}$, and $G_{0}$. These constants and the auxiliary conditions are exported from \Mathematica into a text file and imported into \COMSOL.

\subsection{Solver Setup}

To ensure the solution of our new system, we must make educated decisions about the procedural steps taken by \COMSOL. This regards the size and shape of our mesh, the shape functions chosen to represent the test functions, and most importantly, the time-integration solver. These choices are the factors which will be tested during our verification process. 

\subsubsection{Domain}

The domain chosen for our two-dimensional verification analysis is a simple square of side length \np[m]{1}. We have chosen mapped quadrilateral mesh elements for their ease of implementation. The finite element method retrieves the exact value of the solution at the nodes of the mesh. We interpolate the solution between the nodes via shape functions, typically Lagrange polynomials. In our verification analysis, we have chosen Lagrange linear elements for the pressure and strain in all three formulations. The velocity, displacement, and "mesh motion" were approximated with Lagrange quadratic shape functions.

In the Method of Manufactured Solutions (MMS) \cite{Salari2000}, we define the end solution we wish to obtain. If we define the exact velocity solution to be planar, and then choose linear Lagrange polynomials as its shape functions, it is clear that the exact solution lies within the chosen solution space. This allows us to recover the true solution, no matter the mesh refinement. As we have chosen sinusoidal manufactured solutions, we avoid this issue. 

\subsubsection{Time-Stepping}

The finite element method is used to approximate the problem over a domain at a \emph{fixed point in time}. This reduces the problem to a system of ordinary differential equations in time which is then solved using a finite difference scheme. Our choice for the time-stepping scheme is an IDA solver with a variable-coefficient, variable-order Backward Difference Formula (BDF) to integrate over time. IDA solves a differential-algebraic equation, DAE, system of the following type \cite{Hindmarsh2005}:

%%
\begin{equation}
	\label{eq:IDAFormula}
	F(t,y,\dot{y}) = 0, \quad
	y(t_0) = y_0, \quad
	\dot{y}(t_0) =\dot{y_0}.
\end{equation}
%%
If $y_0$ and $\dot{y_0}$ are not provided by the user, this scheme attempts to compute a set of consistent initial conditions. The specific time integration scheme we used is adaptive BDF of variable order, $q$, between 1 and 5. The order is the number of previous solutions necessary to compute the next time step. The BDF formula is given in \cite{Hindmarsh2005} as
%%
\begin{equation}
	\label{eq:BDF}
	\sum_{i=0}^{q} \alpha_{n,i}y_{n-i}=h_n\dot{y}_n,
\end{equation}
%%
where $y_n$ and $\dot{y}_n$ are the calculated solution and derivative at step $n$, and $h_n$ is the current time step given by $t_n-t_{n-1}$. The coefficients, $\alpha_{n,i}$, depend on the BDF order, $q$, and the past step sizes \cite{Hindmarsh2005}.

There are many benefits to choosing a BDF time integration scheme. It is an adaptive method, meaning it fluctuates its time step size depending on the quality of the previous results. BDF integration is also fully implicit and unconditionally stable. Furthermore, it is of variable order and therefore capable, to some degree, of adapting to the smoothness of the solution. Lastly, observing our system of equations, we can see that there is no differential operator on the field $p$ anywhere, suggesting that our problem is a differential-algebraic; IDA is a differential-algebraic-equation solver (DAE), as mentioned above, designed to handle problems like these.

In summary, our problem is one of an Oldroyd-B type fluid abiding by the conservation of mass, balance of linear momentum, and an evolution law. We have converted the strong form into its corresponding Eulerian, Lagrangian, and ALE weak forms, created arbitrary solutions to these problems, and worked backwards to solve for the necessary source terms, boundary conditions, and initial conditions. We have decided upon the desired time-integration scheme and domain, and implemented the code in \COMSOL. In the next Chapter, we will present the results of these calculations, and discuss the error incurred in each.
