%!TEX root = ../YourName-Dissertation.tex
\chapter{Blood Model Results \& Analysis}

As with the Oldroyd-B fluid model, a two-dimensional verification analysis was carried out of our finite element solver. For each formulation of the problem -- Eulerian, Lagrangian and ALE -- we have performed our verification analysis with six mesh refinements, though only the second, fourth, and last refinements are shown for illustration purposes. The plots in this Chapter are of raw \COMSOL results, with no optical refinement on the resolution.

\section{Eulerian Results}

Recall the plots of our manufactured solution for $p$, $\bv{v}$, and $\tensor{G}$ in Figs.~\ref{fig:ClotMSp}, \ref{fig:ClotMSv1}, and \ref{fig:ClotMSG11} respectively. The corresponding \COMSOL outputs of these fields, with various mesh lengths can be seen in Figs.~\ref{fig:ClotEulerp}--\ref{fig:ClotEulerG}, all at time $t=\np[s]{0.7}$. 

%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotEulerian_p_2} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotEulerian_p_4} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotEulerian_p_6}
\caption{Numerical solution of $p$ in Eulerian framework.
\\
Left to Right: $h=\np[m]{0.125}$, $h=\np[m]{0.0313}$, and $h=\np[m]{7.813e-3}$.}
\label{fig:ClotEulerp}
\end{figure}
%%
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotEulerian_v1_2} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotEulerian_v1_4} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotEulerian_v1_6}
\caption{Numerical solution of $\bv{v}_1$ in Eulerian framework.
\\
Left to Right: $h=\np[m]{0.125}$, $h=\np[m]{0.0313}$, and $h=\np[m]{7.813e-3}$.}
\label{fig:ClotEulerv}
\end{figure}
%%
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotEulerian_G11_2} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotEulerian_G11_4} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotEulerian_G11_6}
\caption{Numerical solution of $\tensor{G}_{11}$ in Eulerian framework.
\\
Left to Right: $h=\np[m]{0.125}$, $h=\np[m]{0.0313}$, and $h=\np[m]{7.813e-3}$.}
\label{fig:ClotEulerG}
\end{figure}
%%

We calculate the $L^2$ and $H^1$ norms of the error between the exact the numerical solutions at various characteristic mesh lengths, $h$. The magnitude of these errors are tabulated in Table~\ref{tab:EulerError1}. Plotting the log of these norms versus the log of the inverse of the mesh length, we obtain Fig.~\ref{fig:ClotEulerConv}. All mesh lengths, $h$, are given in meters.

%%
\begin{table}[h!]
\begin{center}
\renewcommand{\arraystretch}{1.25}
\begin{tabular}{c c c c c c} 
\hline
$h$ [m] & \# d.o.f. & $\| \MS{MSp}-$p$ \|_{L^2}$ & $\| \bv{\MS{MSv}}-\bv{v} \|_{L^2}$ & $\| \bv{\MS{MSv}}-\bv{v} \|_{H^1}$ & $\| \MS{\tensor{MSG}}-\tensor{G} \|_{L^2}$ \\
\hline
\np{0.25} & 262  & $\np{4.836e-4}$     & $\np{4.779e-9}$ & $\np{7.633e-6}$ & $\np{8.213e-5}$ \\
\np{0.125} & 902  & $\np{1.339e-5}$    & $\np{3.962e-10}$ & $\np{4.197e-6}$ & $\np{2.129e-6}$ \\
\np{0.0625} & 3334  & $\np{4.889e-7}$     & $\np{1.215e-12}$ & $\np{7.332e-7}$ & $\np{5.825e-8}$ \\ 
\np{3.125e-2} & 12806 & $\np{1.936e-8}$   & $\np{2.810e-14}$ & $\np{8.646e-9}$ & $\np{1.810e-9}$ \\ 
\np{1.563e-2} & 50182 & $\np{7.879e-10}$     & $\np{2.028e-17}$ & $\np{3.011e-11}$ & $\np{6.274e-11}$ \\ 
\np{7.813e-3} & 198662 & $\np{3.226e-11}$     & $\np{6.028e-20}$ & $\np{1.995e-13}$ & $\np{2.343e-12}$ \\
\hline
\end{tabular}
\end{center}
\caption{Error in numerical solution of $p$, \bv{v}, and \tensor{G} in Eulerian framework.}
\label{tab:ClotEulerError1}
\end{table}
%%

%%
\begin{figure}[h!]
\centering
\includegraphics[scale=0.75]{Chapter-6/Figures/ClotEulerian_Conv}
\caption{Plot of the $L^{2}$ and $H^1$ norms of error for $p$, $\bv{v}$, and $\tensor{G}$ -- Eulerian.}
\label{fig:ClotEulerConv}
\end{figure}
%%

\section{Lagrangian Results}
Our solutions for $p$, $\bv{u}$, and $\tensor{G}$ are given in the previous chapter and their plots can be seen in Figs.~\ref{fig:ClotMSp}, \ref{fig:ClotMSv1}, and \ref{fig:ClotMSG11}, respectively. Below, we present the results outputted by \COMSOL for these three fields in Figs.~\ref{fig:ClotLagp}-\ref{fig:ClotLagG}.
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotLagrangian_p_2} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotLagrangian_p_4} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotLagrangian_p_6}
\caption{Numerical solution of $p$ in Lagrangian framework.
\\
Left to Right: $h=\np[m]{0.125}$, $h=\np[m]{0.0313}$, and $h=\np[m]{7.813e-3}$.}
\label{fig:ClotLagp}
\end{figure}
%%
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.65]{Chapter-6/Figures/ClotLagrangian_u1_2} \hfill
\includegraphics[scale=0.65]{Chapter-6/Figures/ClotLagrangian_u1_4} \hfill
\includegraphics[scale=0.65]{Chapter-6/Figures/ClotLagrangian_u1_6}
\caption{Numerical solution of $\bv{u}_1$ in Lagrangian framework.
\\
Left to Right: $h=\np[m]{0.125}$, $h=\np[m]{0.0313}$, and $h=\np[m]{7.813e-3}$.}
\label{fig:ClotLagu}
\end{figure}
%%
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotLagrangian_G11_2} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotLagrangian_G11_4} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotLagrangian_G11_6}
\caption{Numerical solution of $\tensor{G}_{11}$ in Lagrangian framework.
\\
Left to Right: $h=\np[m]{0.125}$, $h=\np[m]{0.0313}$, and $h=\np[m]{7.813e-3}$.}
\label{fig:ClotLagG}
\end{figure}
%%
We calculate the $L^2$ and $H^1$ norms of the error between the exact the numerical solutions at various characteristic mesh lengths, $h$. The magnitude of these errors are tabulated in Table~\ref{tab:EulerError1}. Plotting the log of these norms versus the log of the inverse of the mesh length, we obtain Fig.~\ref{fig:ClotLagConv}. All mesh lengths, $h$, are given in meters.
%%
\begin{table}[h!]
\begin{center}
\renewcommand{\arraystretch}{1.25}
\begin{tabular}{c c c c c c} 
\hline
$h$ [m] & \# d.o.f. & $\| \MS{MSp}-$p$ \|_{L^2}$ & $\| \bv{\MS{MSu}}-\bv{u} \|_{L^2}$ & $\| \bv{\MS{MSu}}-\bv{u} \|_{H^1}$ & $\| \MS{\tensor{MSG}}-\tensor{G} \|_{L^2}$ \\
\hline
\np{0.25}        & 262       & $\np{4.749e-4}$       & $\np{3.847e-8}$   & $\np{5.603e-5}$   & $\np{3.130e-5}$ \\
\np{0.125}      & 902       & $\np{1.320e-5}$       & $\np{9.211e-11}$  & $\np{1.077e-6}$   & $\np{8.609e-7}$ \\
\np{0.0625}    & 3334     & $\np{4.835e-7}$       & $\np{4.421e-13}$ & $\np{2.875e-8}$   & $\np{3.165e-8}$ \\ 
\np{3.125e-2} & 12806   & $\np{1.927e-8}$       & $\np{3.136e-15}$ & $\np{1.019e-9}$   & $\np{1.263e-9}$ \\ 
\np{1.563e-2} & 50182   & $\np{7.860e-10}$     & $\np{3.280e-17}$ & $\np{4.016e-11}$ & $\np{5.150e-11}$ \\ 
\np{7.813e-3} & 198662 & $\np{3.585e-11}$     & $\np{4.275e-18}$ & $\np{1.634e-12}$ & $\np{2.112e-12}$ \\
\hline
\end{tabular}
\end{center}
\caption{Error in numerical solution of $p$, \bv{u}, and \tensor{G} in Lagrangian framework.}
\label{tab:ClotLagError1}
\end{table}
%%

%%
\begin{figure}[h!]
\centering
\includegraphics[scale=0.75]{Chapter-6/Figures/ClotLagrangian_Conv}
\caption{Plot of the $L^{2}$ and $H^1$ norms of error for $p$, $\bv{u}$, and $\tensor{G}$ -- Lagrangian.}
\label{fig:ClotLagConv}
\end{figure}
%%

\section{ALE Results}

Plots of the exact solutions for $p$, $\bv{v}$, $\hat{\bv{u}}$, and $\tensor{G}$ are given in the previous chapter in Figs.~\ref{fig:ClotMSp}, \ref{fig:ClotMSv1ALE}, \ref{fig:ClotMSv1}, and \ref{fig:ClotMSG11}. The \COMSOL outputs are shown below in Figs.~\ref{fig:ClotALEp}-\ref{fig:ClotALEu} at time $t=\np[s]{0.7}$.

%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotALE_p_2} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotALE_p_4} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotALE_p_6}
\caption{Numerical solution of $p$ in ALE framework.
\\
Left to Right: $h=\np[m]{0.125}$, $h=\np[m]{0.0313}$, and $h=\np[m]{7.813e-3}$.}
\label{fig:ClotALEp}
\end{figure}
%%
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotALE_v1_2} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotALE_v1_4} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotALE_v1_6}
\caption{Numerical solution of $\bv{v}_1$ in ALE framework.
\\
Left to Right: $h=\np[m]{0.125}$, $h=\np[m]{0.0313}$, and $h=\np[m]{7.813e-3}$.}
\label{fig:ClotALEv}
\end{figure}
%%
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotALE_G11_2} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotALE_G11_4} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotALE_G11_6}
\caption{Numerical solution of $\tensor{G}_{11}$ in ALE framework.
\\
Left to Right: $h=\np[m]{0.125}$, $h=\np[m]{0.0313}$, and $h=\np[m]{7.813e-3}$.}
\label{fig:ClotALEG}
\end{figure}
%%
%%
\begin{figure}[h!]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotALE_u1_2} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotALE_u1_4} \hfill
\includegraphics[scale=0.66]{Chapter-6/Figures/ClotALE_u1_6}
\caption{Numerical solution of $\hat{\bv{u}}_1$ in ALE framework.
\\
Left to Right: $h=\np[m]{0.125}$, $h=\np[m]{0.0313}$, and $h=\np[m]{7.813e-3}$.}
\label{fig:ClotALEu}
\end{figure}
%%

We calculate the $L^2$ and $H^1$ norms of the error between the exact the numerical solutions at various characteristic mesh lengths, $h$. The magnitude of these errors are tabulated in Table~\ref{tab:EulerError1}. Plotting the log of these norms versus the log of the inverse of the mesh length, we obtain Fig.~\ref{fig:ClotALEConv}. All mesh lengths, $h$, are given in meters.

%%
\begin{table}[h!]
\begin{center}
\renewcommand{\arraystretch}{1.25}
\begin{tabular}{c c c c c c} 
\hline
$h$ [m] & \# d.o.f. & $\| \MS{MSp}-$p$ \|_{L^2}$ & $\| \bv{\MS{MSv}}-\bv{v} \|_{L^2}$ & $\| \bv{\MS{MSv}}-\bv{v} \|_{H^1}$ & $\| \MS{\tensor{MSG}}-\tensor{G} \|_{L^2}$ \\
\hline
\np{0.25}        & 592       & $\np{1.973e-1}$       & $\np{1.444e-7}$   & $\np{3.780e-4}$   & $\np{3.095e-5}$ \\
\np{0.125}      & 2104     & $\np{2.360e-4}$       & $\np{8.303e-10}$ & $\np{1.163e-5}$   & $\np{8.638e-7}$ \\
\np{0.0625}    & 7912     & $\np{9.105e-7}$       & $\np{1.994e-11}$ & $\np{1.201e-6}$   & $\np{3.167e-8}$ \\ 
\np{3.125e-2} & 30664   & $\np{2.036e-8}$       & $\np{9.655e-13}$ & $\np{2.936e-7}$   & $\np{1.263e-9}$ \\ 
\np{1.563e-2} & 120712 & $\np{1.664e-8}$       & $\np{2.496e-14}$ & $\np{3.232e-8}$   & $\np{5.168e-11}$ \\ 
\np{7.813e-3} & 198662 & $\np{3.298e-11}$     & $\np{6.171e-16}$ & $\np{3.325e-10}$ & $\np{2.124e-12}$ \\
\hline
\end{tabular}
\end{center}
\caption{Error in numerical solution of $p$, \bv{v}, and \tensor{G} in ALE framework.}
\label{tab:ClotALEError1}
\end{table}
%%
%%
\begin{table}[h!]
\begin{center}
\renewcommand{\arraystretch}{1.25}
\begin{tabular}{c c c c} 
\hline
$h$ & \# d.o.f. & $\| \MS{\bv{MS\hat{u}}}-\hat{\bv{u}} \|_{L^2}$ & $\| \MS{\bv{MS\hat{u}}}-\hat{\bv{u}} \|_{H^1}$  \\ \hline
\np{0.25}        & 592       & $\np{4.532e-9}$      & $\np{1.345e-5}$ \\
\np{0.125}      & 2104     & $\np{4.145e-11}$    & $\np{5.670e-7}$ \\
\np{0.0625}    & 7912     & $\np{3.530e-13}$    & $\np{2.341e-8}$ \\ 
\np{3.125e-2} & 30664.  & $\np{2.954e-15}$    & $\np{9.629e-10}$ \\ 
\np{1.563e-2} & 120712 & $\np{2.876e-17}$    & $\np{3.958e-11}$ \\ 
\np{7.813e-3} & 198662 & $\np{2.094e-19}$    & $\np{1.626e-12}$ \\
\hline
\end{tabular}
\end{center}
\caption{Error in numerical solution of \bv{\hat{u}} in ALE framework.}
\label{tab:ClotALEError2}
\end{table}
%%

%%
\begin{figure}[h!]
\centering
\includegraphics[scale=0.75]{Chapter-6/Figures/ClotALE_Conv}
\caption{Plot of the $L^{2}$ and $H^1$ norms of error for $p$, $\bv{v}$, $\hat{\bv{u}}$, and $\tensor{G}$ -- ALE.}
\label{fig:ClotALEConv}
\end{figure}
%%

\section{Analysis}

As with the Oldroyd-B fluid model problem, we can qualitatively observe the smoothing of the solution with refinement of the mesh. In this chapter, we have only displayed the second, fourth, and sixth mesh refinements, simply for the purpose of visualization. The analysis of this apparent convergence begins with the quantitative data. As we know the exact solutions, we have calculated the $L^2$ and $H^1$ norms of the error between the exact and numerical solutions at each mesh length. The magnitude of these errors are presented in Tables~\ref{tab:ClotEulerError1}-\ref{tab:ClotALEError2}. The error magnitudes in all three formulations are relatively small and decreasing, with the solutions for $\bv{v}$, $\bv{u}$, and $\bv{\hat{u}}$ performing the best. In the Eulerian case, the error in the solution for $\bv{v}$ is on the order of $10^{-20}$. Plotting the log of these norms versus the log of the inverse of the mesh length, we obtain Figs.~\ref{fig:ClotEulerConv}, \ref{fig:ClotLagConv}, and \ref{fig:ClotALEConv}.

Beginning with the Eulerian convergence plot, the $L^2$ error in $p$ and $\tensor{G}$ follow the expected trend of convergence at a rate of 2. The velocity solution, however, converges at a rate one lower than the expected: 2 in the $L^2$ norm and 1 in the $H^1$ norm. This continues steadily until the sixth mesh refinement where the error drops with the expected rates of 3 and 2 in the $L^2$ and $H^1$ norms of error, respectively. The Lagrangian plot depicts the expected convergences up until the sixth refinement. Between the fifth and sixth refinements, the $L^2$ norm of the error in the displacement does not decrease as much as predicted. The actual value of the error in this quantity is on the order of $10^{-18}$ by the last step. Finally we turn our attention to the ALE results. Fig.~\ref{fig:ClotALEConv} exhibits regular convergence of $\bv{\hat{u}}$ and $\tensor{G}$. The velocity converges at an approximate rate that is lower than expected: slope of 1 for the $H^1$ norm of error and a slope of 2 for the $L^2$ norm. The pressure does approach the true solution with a finer mesh, as in the error monotonically decreases with refinement, but the rate at which it does so is unclear. More work needs to be done to ensure consistent convergence rates of these quantities in the clot problem.

